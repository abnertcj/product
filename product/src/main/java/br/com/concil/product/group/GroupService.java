package br.com.concil.product.group;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.concil.product.group.repository.Group;
import br.com.concil.product.group.repository.GroupRepository;

@Service
public class GroupService {

	@Autowired
	private GroupRepository repository;
	
	public Group findById(Long id) {
		return repository.findOne(id);
	}
	
	public List<Group> findAll() {
		return repository.findAll();
	}
	
	public Group save(Group group) {
		group.setCreated(new Date());
		return repository.save(group);
	}
	
	public Group update(Long id, Group group) {
		group.setUpdated(new Date());
		return repository.saveAndFlush(group);
	}
	
	public void delete(Long id) {
		repository.delete(id);
	}
	
	public void deleteAll() {
		repository.deleteAll();
	}
	
}
