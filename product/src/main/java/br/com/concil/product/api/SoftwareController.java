package br.com.concil.product.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.concil.product.product.ProductService;
import br.com.concil.product.product.SoftwareService;
import br.com.concil.product.product.repository.Product;
import br.com.concil.product.product.repository.Software;
import br.com.concil.product.tax.repository.Tax;

@RestController
public class SoftwareController {
	
	@Autowired
	private SoftwareService service;
	
	@Autowired
	private ProductService productService;

	@RequestMapping(value = "/api/software/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Software> findById(@PathVariable("id") Long id) {
		Software software = service.findById(id);
		if (software == null)
			return new ResponseEntity<Software>(software, HttpStatus.NOT_FOUND);
		return new ResponseEntity<Software>(software, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/software", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Software>> findAll() {
		List<Software> softwares = service.findAll();
		if (softwares.isEmpty())
			return new ResponseEntity<List<Software>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Software>>(softwares, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/software", method = RequestMethod.POST)
	public ResponseEntity<Software> save(@RequestBody Software software) {
		List<Tax> taxes = software.getTaxes();
		software.setTaxes(null);
		
		Product product = software.getProduct();
		Product productSaved = productService.save(product);
		software.setProduct(null);
		
		Software softwareSavedWithoutRelationships = service.save(software);
		
		softwareSavedWithoutRelationships.setTaxes(taxes);
		softwareSavedWithoutRelationships.setProduct(productSaved);
		Software softwareSaved = service.update(softwareSavedWithoutRelationships.getId(), softwareSavedWithoutRelationships);
		
		return new ResponseEntity<Software>(softwareSaved, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/software/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Software> update(@PathVariable Long id, @RequestBody Software software) {
		List<Tax> taxes = software.getTaxes();
		software.setTaxes(null);
		
		Software softwareSaved = service.findById(id);
		Product productSaved = softwareSaved.getProduct();
		if (productSaved != null) {
			software.getProduct().setId(productSaved.getId());
			productService.update(productSaved.getId(), software.getProduct());
		}
		
		Software softwareUpdatedWithoutRelationships = service.update(id, software);
		
		softwareUpdatedWithoutRelationships.setTaxes(taxes);
		Software softwareUpdated = service.update(softwareUpdatedWithoutRelationships.getId(), softwareUpdatedWithoutRelationships);
		
		return new ResponseEntity<Software>(softwareUpdated, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/software/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Software> delete(@PathVariable("id") Long id) {
		Software software = service.findById(id);
		Product product = software.getProduct();
		
		service.delete(id);
		productService.delete(product.getId());
		return new ResponseEntity<Software>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/software", method = RequestMethod.DELETE)
	public ResponseEntity<Software> deleteAll() {
		service.deleteAll();
		return new ResponseEntity<Software>(HttpStatus.OK);
	}
	
}