package br.com.concil.product.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.concil.product.matrix.MatrixService;
import br.com.concil.product.matrix.repository.Matrix;
import br.com.concil.product.sku.repository.Package;

@RestController
public class MatrixController {
	
	@Autowired
	private MatrixService service;

	@RequestMapping(value = "/api/matrix/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Matrix> findById(@PathVariable("id") Long id) {
		Matrix matrix = service.findById(id);
		if (matrix == null)
			return new ResponseEntity<Matrix>(matrix, HttpStatus.NOT_FOUND);
		return new ResponseEntity<Matrix>(matrix, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/matrix", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Matrix>> findAll() {
		List<Matrix> matrixes = service.findAll();
		if (matrixes.isEmpty())
			return new ResponseEntity<List<Matrix>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Matrix>>(matrixes, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/matrix", method = RequestMethod.POST)
	public ResponseEntity<Matrix> save(@RequestBody Matrix matrix) {
		Matrix matrixSaved = service.save(matrix);
		return new ResponseEntity<Matrix>(matrixSaved, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/matrix/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Matrix> update(@PathVariable Long id, @RequestBody Matrix matrix) {
		Package pack = matrix.getPackage();
		matrix.setPackage(null);
		
		Matrix matrixWithoutRelationships = service.update(id, matrix);
		Matrix matrixUpdated = null;
		
		if (pack != null) {
			matrixWithoutRelationships.setPackage(pack);
			matrixUpdated = service.update(matrixWithoutRelationships.getId(), matrixWithoutRelationships);
		} else {
			matrixUpdated = matrixWithoutRelationships;
		}
		
		return new ResponseEntity<Matrix>(matrixUpdated, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/matrix/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Matrix> delete(@PathVariable("id") Long id) {
		service.delete(id);
		return new ResponseEntity<Matrix>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/matrix", method = RequestMethod.DELETE)
	public ResponseEntity<Matrix> deleteAll() {
		service.deleteAll();
		return new ResponseEntity<Matrix>(HttpStatus.OK);
	}
	
}