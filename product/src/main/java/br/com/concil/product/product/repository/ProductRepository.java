package br.com.concil.product.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ProductRepository extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {

//	@Query("Select s from SalesOrder s where s.saleDate >= ?1 and s.saleDate <= ?2")
//	public List<Product> findAllInRangeDate(Date startDate, Date endDate);
}
