package br.com.concil.product.product;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.concil.product.product.repository.Product;
import br.com.concil.product.product.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	private ProductRepository repository;
	
	public Product findById(Long id) {
		return repository.findOne(id);
	}
	
	public List<Product> findAll() {
//		StandardProductSpecificationBuilder spec = new StandardProductSpecificationBuilder();
//		return repository.findAll(spec.build(new Object()));
		return repository.findAll();
	}
	
	public Product save(Product product) {
		product.setCreated(new Date());
		return repository.save(product);
	}
	
	public Product update(Long id, Product product) {
		product.setUpdated(new Date());
		return repository.saveAndFlush(product);
	}
	
	public void delete(Long id) {
		repository.delete(id);
	}
	
	public void deleteAll() {
		repository.deleteAll();
	}
	
}
