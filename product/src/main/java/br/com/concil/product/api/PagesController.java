package br.com.concil.product.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PagesController {

	@RequestMapping(value = {"/", "index"})
	public String test() {
		return "products";
	}
	
	@RequestMapping(value = {"products"})
	public String getProducts() {
		return "products";
	}
	
	@RequestMapping(value = {"addons"})
	public String getAddons() {
		return "addons";
	}
	
	@RequestMapping(value = {"groups"})
	public String getGroups() {
		return "groups";
	}
	
	@RequestMapping(value = {"taxes"})
	public String getFees() {
		return "taxes";
	}
	
	@RequestMapping(value = {"packages"})
	public String getPackages() {
		return "packages";
	}
	
	@RequestMapping(value = {"matrix"})
	public String getMatrix() {
		return "matrix";
	}
	
}