package br.com.concil.product.product.repository;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.concil.product.addon.repository.Addon;
import br.com.concil.product.tax.repository.Tax;

@Entity
@Table(name = "TB_SOFTWARE")
public class Software implements Serializable {

	private static final long serialVersionUID = 8681291735630290273L;

	@Id
	@Column(name = "ID")
	@GeneratedValue
	private Long id;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_PRODUCT")
	private Product product;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.MERGE)
    @JoinTable(name="TB_SOFTWARE_ADDON", joinColumns=@JoinColumn(name="ID_SOFTWARE"), inverseJoinColumns=@JoinColumn(name="ID_ADDON"))
	private List<Addon> addons;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.MERGE)
    @JoinTable(name="TB_SOFTWARE_TAX", joinColumns=@JoinColumn(name="ID_SOFTWARE"), inverseJoinColumns=@JoinColumn(name="ID_TAX"))
	private List<Tax> taxes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public List<Addon> getAddons() {
		return addons;
	}

	public void setAddons(List<Addon> addons) {
		this.addons = addons;
	}

	public List<Tax> getTaxes() {
		return taxes;
	}

	public void setTaxes(List<Tax> taxes) {
		this.taxes = taxes;
	}
	
}
