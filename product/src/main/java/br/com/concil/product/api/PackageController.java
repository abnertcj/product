package br.com.concil.product.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.concil.product.product.repository.Product;
import br.com.concil.product.sku.PackageService;
import br.com.concil.product.sku.repository.Package;

@RestController
public class PackageController {
	
	@Autowired
	private PackageService service;

	@RequestMapping(value = "/api/package/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Package> findById(@PathVariable("id") Long id) {
		Package packages = service.findById(id);
		if (packages == null)
			return new ResponseEntity<Package>(packages, HttpStatus.NOT_FOUND);
		return new ResponseEntity<Package>(packages, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/package", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Package>> findAll() {
		List<Package> packages = service.findAll();
		if (packages.isEmpty())
			return new ResponseEntity<List<Package>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Package>>(packages, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/package", method = RequestMethod.POST)
	public ResponseEntity<Package> save(@RequestBody Package pack) {
		Product product = pack.getProduct();
		pack.setProduct(null);
		Package packageSavedWithoutRelationships = service.save(pack);
		
		Package packageSaved = null;
		if (product != null) {
			packageSavedWithoutRelationships.setProduct(product);
			packageSaved = service.save(packageSavedWithoutRelationships);
		} else {
			packageSaved = packageSavedWithoutRelationships;
		}
		
		return new ResponseEntity<Package>(packageSaved, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/package/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Package> update(@PathVariable Long id, @RequestBody Package pack) {
		Product product = pack.getProduct();
		pack.setProduct(null);
		Package packageSavedWithoutRelationships = service.update(id, pack);
		Package packageUpdated = null;
		
		if (product != null) {
			packageSavedWithoutRelationships.setProduct(product);
			packageUpdated = service.update(packageSavedWithoutRelationships.getId(), packageSavedWithoutRelationships);
		} else {
			packageUpdated = packageSavedWithoutRelationships;
		}
		
		return new ResponseEntity<Package>(packageUpdated, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/package/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Package> delete(@PathVariable("id") Long id) {
		service.delete(id);
		return new ResponseEntity<Package>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/package", method = RequestMethod.DELETE)
	public ResponseEntity<Package> deleteAll() {
		service.deleteAll();
		return new ResponseEntity<Package>(HttpStatus.OK);
	}
	
}