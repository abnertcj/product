package br.com.concil.product.addon;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.concil.product.addon.repository.Addon;
import br.com.concil.product.addon.repository.AddonRepository;

@Service
public class AddonService {

	@Autowired
	private AddonRepository repository;
	
	public Addon findById(Long id) {
		return repository.findOne(id);
	}
	
	public List<Addon> findAll() {
		return repository.findAll();
	}
	
	public Addon save(Addon addon) {
		return repository.save(addon);
	}
	
	public Addon update(Long id, Addon addon) {
		return repository.saveAndFlush(addon);
	}
	
	public void delete(Long id) {
		repository.delete(id);
	}
	
	public void deleteAll() {
		repository.deleteAll();
	}
	
}
