package br.com.concil.product.product.repository;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

public class StandardProductSpecificationBuilder {

	public Specification<Software> build(Object parameter) {
		return Specifications.where(new ProductSpecification<Software>());
//		return Specifications.where(new CompanySpecification<Product>(parameter)
//				.and(new AcquirerSpecification<SalesOrder>(parameter));
	}
}
