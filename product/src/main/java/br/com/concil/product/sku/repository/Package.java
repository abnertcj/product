package br.com.concil.product.sku.repository;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.concil.product.product.repository.Product;


@Entity
@Table(name = "TB_PACKAGE")
public class Package implements Serializable {

	private static final long serialVersionUID = -4588458266243742218L;

	@Id
	@Column(name = "ID")
	@GeneratedValue
	private Long id;
	
	@Column(name = "NAME")
	private String name;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_PRODUCT")
	private Product product;
	
	@Column(name = "VOLUME")
	private Integer volume;
	
	@Column(name = "UNIT")
	private Integer unit;
	
	@Column(name = "PRICE")
	private Double price;
	
	@Column(name = "RECURRENT")
	private String recurrent;
	
	@Column(name = "SETUP")
	private String setup;
	
	@Column(name = "CUSTOM")
	private String custom;
	
	@Column(name = "SETUP_PRICE")
	private Double setupPrice;
	
	@Column(name = "RECURRENCE_PRICE")
	private Double recurrencePrice;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created", unique = true, nullable = false)
	private Date created;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated", unique = true, nullable = false)
	private Date updated;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

//	public List<Matrix> getMatrixes() {
//		return matrixes;
//	}
//
//	public void setMatrixes(List<Matrix> matrixes) {
//		this.matrixes = matrixes;
//	}
	
//	public Long getIdMatrix() {
//		return idMatrix;
//	}
//	
//	public void setIdMatrix(Long idMatrix) {
//		this.idMatrix = idMatrix;
//	}

	public Integer getVolume() {
		return volume;
	}

	public void setVolume(Integer volume) {
		this.volume = volume;
	}

	public Integer getUnit() {
		return unit;
	}

	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getRecurrent() {
		return recurrent;
	}

	public void setRecurrent(String recurrent) {
		this.recurrent = recurrent;
	}

	public String getSetup() {
		return setup;
	}

	public void setSetup(String setup) {
		this.setup = setup;
	}

	public String getCustom() {
		return custom;
	}

	public void setCustom(String custom) {
		this.custom = custom;
	}

	public Double getSetupPrice() {
		return setupPrice;
	}

	public void setSetupPrice(Double setupPrice) {
		this.setupPrice = setupPrice;
	}

	public Double getRecurrencePrice() {
		return recurrencePrice;
	}

	public void setRecurrencePrice(Double recurrencePrice) {
		this.recurrencePrice = recurrencePrice;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

}
