package br.com.concil.product.matrix.repository;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.concil.product.sku.repository.Package;

@Entity
@Table(name = "TB_MATRIX")
public class Matrix implements Serializable {

	/*
	 * Inativa, ativa e draft
	 */

	private static final long serialVersionUID = -2966940257006847678L;

	@Id
	@Column(name = "ID")
	@GeneratedValue
	private Long id;
	
	@Column(name = "NAME")
	private String name;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "ID_PACKAGE")
	private Package pack;
	
	/* BRL */
	
	// ALLOCATED
	
	// CAPEX
	// Valores Pontuais
	@Column(name = "BRL_ALLOCATED_CAPEX_INSTALL")
	private Double brlAllocatedCapexInstall;
	
	@Column(name = "BRL_ALLOCATED_CAPEX_PONT_1")
	private Double brlAllocatedCapexPont1;
	
	@Column(name = "BRL_ALLOCATED_CAPEX_PONT_13")
	private Double brlAllocatedCapexPont13;
	
	@Column(name = "BRL_ALLOCATED_CAPEX_PONT_25")
	private Double brlAllocatedCapexPont25;
	
	@Column(name = "BRL_ALLOCATED_CAPEX_PONT_37")
	private Double brlAllocatedCapexPont37;
	
	@Column(name = "BRL_ALLOCATED_CAPEX_PONT_49")
	private Double brlAllocatedCapexPont49;
	
	// Valores Mensais confirme Prazo Contratual
	@Column(name = "BRL_ALLOCATED_CAPEX_MENS_12")
	private Double brlAllocatedCapexMens12;
	
	@Column(name = "BRL_ALLOCATED_CAPEX_MENS_24")
	private Double brlAllocatedCapexMens24;
	
	@Column(name = "BRL_ALLOCATED_CAPEX_MENS_36")
	private Double brlAllocatedCapexMens36;
	
	@Column(name = "BRL_ALLOCATED_CAPEX_MENS_48")
	private Double brlAllocatedCapexMens48;

	@Column(name = "BRL_ALLOCATED_CAPEX_MENS_60")
	private Double brlAllocatedCapexMens60;
	
	// OPEX
	// Valores Pontuais
	@Column(name = "BRL_ALLOCATED_OPEX_INSTALL")
	private Double brlAllocatedOpexInstall;
	
	@Column(name = "BRL_ALLOCATED_OPEX_PONT_1")
	private Double brlAllocatedOpexPont1;
	
	@Column(name = "BRL_ALLOCATED_OPEX_PONT_13")
	private Double brlAllocatedOpexPont13;
	
	@Column(name = "BRL_ALLOCATED_OPEX_PONT_25")
	private Double brlAllocatedOpexPont25;
	
	@Column(name = "BRL_ALLOCATED_OPEX_PONT_37")
	private Double brlAllocatedOpexPont37;
	
	@Column(name = "BRL_ALLOCATED_OPEX_PONT_49")
	private Double brlAllocatedOpexPont49;
	
	// Valores Mensais confirme Prazo Contratual
	@Column(name = "BRL_ALLOCATED_OPEX_MENS_12")
	private Double brlAllocatedOpexMens12;
	
	@Column(name = "BRL_ALLOCATED_OPEX_MENS_24")
	private Double brlAllocatedOpexMens24;
	
	@Column(name = "BRL_ALLOCATED_OPEX_MENS_36")
	private Double brlAllocatedOpexMens36;
	
	@Column(name = "BRL_ALLOCATED_OPEX_MENS_48")
	private Double brlAllocatedOpexMens48;

	@Column(name = "BRL_ALLOCATED_OPEX_MENS_60")
	private Double brlAllocatedOpexMens60;
	
	// COGS
	// Valores Pontuais
	@Column(name = "BRL_ALLOCATED_COGS_INSTALL")
	private Double brlAllocatedCogsInstall;
	
	@Column(name = "BRL_ALLOCATED_COGS_PONT_1")
	private Double brlAllocatedCogsPont1;
	
	@Column(name = "BRL_ALLOCATED_COGS_PONT_13")
	private Double brlAllocatedCogsPont13;
	
	@Column(name = "BRL_ALLOCATED_COGS_PONT_25")
	private Double brlAllocatedCogsPont25;
	
	@Column(name = "BRL_ALLOCATED_COGS_PONT_37")
	private Double brlAllocatedCogsPont37;
	
	@Column(name = "BRL_ALLOCATED_COGS_PONT_49")
	private Double brlAllocatedCogsPont49;
	
	// Valores Mensais confirme Prazo Contratual
	@Column(name = "BRL_ALLOCATED_COGS_MENS_12")
	private Double brlAllocatedCogsMens12;
	
	@Column(name = "BRL_ALLOCATED_COGS_MENS_24")
	private Double brlAllocatedCogsMens24;
	
	@Column(name = "BRL_ALLOCATED_COGS_MENS_36")
	private Double brlAllocatedCogsMens36;
	
	@Column(name = "BRL_ALLOCATED_COGS_MENS_48")
	private Double brlAllocatedCogsMens48;

	@Column(name = "BRL_ALLOCATED_COGS_MENS_60")
	private Double brlAllocatedCogsMens60;
	
	// ACCOMPLISHED
	
	// CAPEX
	// Valores Pontuais
	@Column(name = "BRL_ACCOMPLISHED_CAPEX_INSTALL")
	private Double brlAccomplishedCapexInstall;
	
	@Column(name = "BRL_ACCOMPLISHED_CAPEX_PONT_1")
	private Double brlAccomplishedCapexPont1;
	
	@Column(name = "BRL_ACCOMPLISHED_CAPEX_PONT_13")
	private Double brlAccomplishedCapexPont13;
	
	@Column(name = "BRL_ACCOMPLISHED_CAPEX_PONT_25")
	private Double brlAccomplishedCapexPont25;
	
	@Column(name = "BRL_ACCOMPLISHED_CAPEX_PONT_37")
	private Double brlAccomplishedCapexPont37;
	
	@Column(name = "BRL_ACCOMPLISHED_CAPEX_PONT_49")
	private Double brlAccomplishedCapexPont49;
	
	// Valores Mensais confirme Prazo Contratual
	@Column(name = "BRL_ACCOMPLISHED_CAPEX_MENS_12")
	private Double brlAccomplishedCapexMens12;
	
	@Column(name = "BRL_ACCOMPLISHED_CAPEX_MENS_24")
	private Double brlAccomplishedCapexMens24;
	
	@Column(name = "BRL_ACCOMPLISHED_CAPEX_MENS_36")
	private Double brlAccomplishedCapexMens36;
	
	@Column(name = "BRL_ACCOMPLISHED_CAPEX_MENS_48")
	private Double brlAccomplishedCapexMens48;

	@Column(name = "BRL_ACCOMPLISHED_CAPEX_MENS_60")
	private Double brlAccomplishedCapexMens60;
	
	// OPEX
	// Valores Pontuais
	@Column(name = "BRL_ACCOMPLISHED_OPEX_INSTALL")
	private Double brlAccomplishedOpexInstall;
	
	@Column(name = "BRL_ACCOMPLISHED_OPEX_PONT_1")
	private Double brlAccomplishedOpexPont1;
	
	@Column(name = "BRL_ACCOMPLISHED_OPEX_PONT_13")
	private Double brlAccomplishedOpexPont13;
	
	@Column(name = "BRL_ACCOMPLISHED_OPEX_PONT_25")
	private Double brlAccomplishedOpexPont25;
	
	@Column(name = "BRL_ACCOMPLISHED_OPEX_PONT_37")
	private Double brlAccomplishedOpexPont37;
	
	@Column(name = "BRL_ACCOMPLISHED_OPEX_PONT_49")
	private Double brlAccomplishedOpexPont49;
	
	// Valores Mensais confirme Prazo Contratual
	@Column(name = "BRL_ACCOMPLISHED_OPEX_MENS_12")
	private Double brlAccomplishedOpexMens12;
	
	@Column(name = "BRL_ACCOMPLISHED_OPEX_MENS_24")
	private Double brlAccomplishedOpexMens24;
	
	@Column(name = "BRL_ACCOMPLISHED_OPEX_MENS_36")
	private Double brlAccomplishedOpexMens36;
	
	@Column(name = "BRL_ACCOMPLISHED_OPEX_MENS_48")
	private Double brlAccomplishedOpexMens48;

	@Column(name = "BRL_ACCOMPLISHED_OPEX_MENS_60")
	private Double brlAccomplishedOpexMens60;
	
	// COGS
	// Valores Pontuais
	@Column(name = "BRL_ACCOMPLISHED_COGS_INSTALL")
	private Double brlAccomplishedCogsInstall;
	
	@Column(name = "BRL_ACCOMPLISHED_COGS_PONT_1")
	private Double brlAccomplishedCogsPont1;
	
	@Column(name = "BRL_ACCOMPLISHED_COGS_PONT_13")
	private Double brlAccomplishedCogsPont13;
	
	@Column(name = "BRL_ACCOMPLISHED_COGS_PONT_25")
	private Double brlAccomplishedCogsPont25;
	
	@Column(name = "BRL_ACCOMPLISHED_COGS_PONT_37")
	private Double brlAccomplishedCogsPont37;
	
	@Column(name = "BRL_ACCOMPLISHED_COGS_PONT_49")
	private Double brlAccomplishedCogsPont49;
	
	// Valores Mensais confirme Prazo Contratual
	@Column(name = "BRL_ACCOMPLISHED_COGS_MENS_12")
	private Double brlAccomplishedCogsMens12;
	
	@Column(name = "BRL_ACCOMPLISHED_COGS_MENS_24")
	private Double brlAccomplishedCogsMens24;
	
	@Column(name = "BRL_ACCOMPLISHED_COGS_MENS_36")
	private Double brlAccomplishedCogsMens36;
	
	@Column(name = "BRL_ACCOMPLISHED_COGS_MENS_48")
	private Double brlAccomplishedCogsMens48;

	@Column(name = "BRL_ACCOMPLISHED_COGS_MENS_60")
	private Double brlAccomplishedCogsMens60;
	
	/* USD */
	
	// ALLOCATED
	
	// CAPEX
	// Valores Pontuais
	@Column(name = "USD_ALLOCATED_CAPEX_INSTALL")
	private Double usdAllocatedCapexInstall;
	
	@Column(name = "USD_ALLOCATED_CAPEX_PONT_1")
	private Double usdAllocatedCapexPont1;
	
	@Column(name = "USD_ALLOCATED_CAPEX_PONT_13")
	private Double usdAllocatedCapexPont13;
	
	@Column(name = "USD_ALLOCATED_CAPEX_PONT_25")
	private Double usdAllocatedCapexPont25;
	
	@Column(name = "USD_ALLOCATED_CAPEX_PONT_37")
	private Double usdAllocatedCapexPont37;
	
	@Column(name = "USD_ALLOCATED_CAPEX_PONT_49")
	private Double usdAllocatedCapexPont49;
	
	// Valores Mensais confirme Prazo Contratual
	@Column(name = "USD_ALLOCATED_CAPEX_MENS_12")
	private Double usdAllocatedCapexMens12;
	
	@Column(name = "USD_ALLOCATED_CAPEX_MENS_24")
	private Double usdAllocatedCapexMens24;
	
	@Column(name = "USD_ALLOCATED_CAPEX_MENS_36")
	private Double usdAllocatedCapexMens36;
	
	@Column(name = "USD_ALLOCATED_CAPEX_MENS_48")
	private Double usdAllocatedCapexMens48;

	@Column(name = "USD_ALLOCATED_CAPEX_MENS_60")
	private Double usdAllocatedCapexMens60;
	
	// OPEX
	// Valores Pontuais
	@Column(name = "USD_ALLOCATED_OPEX_INSTALL")
	private Double usdAllocatedOpexInstall;
	
	@Column(name = "USD_ALLOCATED_OPEX_PONT_1")
	private Double usdAllocatedOpexPont1;
	
	@Column(name = "USD_ALLOCATED_OPEX_PONT_13")
	private Double usdAllocatedOpexPont13;
	
	@Column(name = "USD_ALLOCATED_OPEX_PONT_25")
	private Double usdAllocatedOpexPont25;
	
	@Column(name = "USD_ALLOCATED_OPEX_PONT_37")
	private Double usdAllocatedOpexPont37;
	
	@Column(name = "USD_ALLOCATED_OPEX_PONT_49")
	private Double usdAllocatedOpexPont49;
	
	// Valores Mensais confirme Prazo Contratual
	@Column(name = "USD_ALLOCATED_OPEX_MENS_12")
	private Double usdAllocatedOpexMens12;
	
	@Column(name = "USD_ALLOCATED_OPEX_MENS_24")
	private Double usdAllocatedOpexMens24;
	
	@Column(name = "USD_ALLOCATED_OPEX_MENS_36")
	private Double usdAllocatedOpexMens36;
	
	@Column(name = "USD_ALLOCATED_OPEX_MENS_48")
	private Double usdAllocatedOpexMens48;

	@Column(name = "USD_ALLOCATED_OPEX_MENS_60")
	private Double usdAllocatedOpexMens60;
	
	// COGS
	// Valores Pontuais
	@Column(name = "USD_ALLOCATED_COGS_INSTALL")
	private Double usdAllocatedCogsInstall;
	
	@Column(name = "USD_ALLOCATED_COGS_PONT_1")
	private Double usdAllocatedCogsPont1;
	
	@Column(name = "USD_ALLOCATED_COGS_PONT_13")
	private Double usdAllocatedCogsPont13;
	
	@Column(name = "USD_ALLOCATED_COGS_PONT_25")
	private Double usdAllocatedCogsPont25;
	
	@Column(name = "USD_ALLOCATED_COGS_PONT_37")
	private Double usdAllocatedCogsPont37;
	
	@Column(name = "USD_ALLOCATED_COGS_PONT_49")
	private Double usdAllocatedCogsPont49;
	
	// Valores Mensais confirme Prazo Contratual
	@Column(name = "USD_ALLOCATED_COGS_MENS_12")
	private Double usdAllocatedCogsMens12;
	
	@Column(name = "USD_ALLOCATED_COGS_MENS_24")
	private Double usdAllocatedCogsMens24;
	
	@Column(name = "USD_ALLOCATED_COGS_MENS_36")
	private Double usdAllocatedCogsMens36;
	
	@Column(name = "USD_ALLOCATED_COGS_MENS_48")
	private Double usdAllocatedCogsMens48;

	@Column(name = "USD_ALLOCATED_COGS_MENS_60")
	private Double usdAllocatedCogsMens60;
	
	// ACCOMPLISHED
	
	// CAPEX
	// Valores Pontuais
	@Column(name = "USD_ACCOMPLISHED_CAPEX_INSTALL")
	private Double usdAccomplishedCapexInstall;
	
	@Column(name = "USD_ACCOMPLISHED_CAPEX_PONT_1")
	private Double usdAccomplishedCapexPont1;
	
	@Column(name = "USD_ACCOMPLISHED_CAPEX_PONT_13")
	private Double usdAccomplishedCapexPont13;
	
	@Column(name = "USD_ACCOMPLISHED_CAPEX_PONT_25")
	private Double usdAccomplishedCapexPont25;
	
	@Column(name = "USD_ACCOMPLISHED_CAPEX_PONT_37")
	private Double usdAccomplishedCapexPont37;
	
	@Column(name = "USD_ACCOMPLISHED_CAPEX_PONT_49")
	private Double usdAccomplishedCapexPont49;
	
	// Valores Mensais confirme Prazo Contratual
	@Column(name = "USD_ACCOMPLISHED_CAPEX_MENS_12")
	private Double usdAccomplishedCapexMens12;
	
	@Column(name = "USD_ACCOMPLISHED_CAPEX_MENS_24")
	private Double usdAccomplishedCapexMens24;
	
	@Column(name = "USD_ACCOMPLISHED_CAPEX_MENS_36")
	private Double usdAccomplishedCapexMens36;
	
	@Column(name = "USD_ACCOMPLISHED_CAPEX_MENS_48")
	private Double usdAccomplishedCapexMens48;

	@Column(name = "USD_ACCOMPLISHED_CAPEX_MENS_60")
	private Double usdAccomplishedCapexMens60;
	
	// OPEX
	// Valores Pontuais
	@Column(name = "USD_ACCOMPLISHED_OPEX_INSTALL")
	private Double usdAccomplishedOpexInstall;
	
	@Column(name = "USD_ACCOMPLISHED_OPEX_PONT_1")
	private Double usdAccomplishedOpexPont1;
	
	@Column(name = "USD_ACCOMPLISHED_OPEX_PONT_13")
	private Double usdAccomplishedOpexPont13;
	
	@Column(name = "USD_ACCOMPLISHED_OPEX_PONT_25")
	private Double usdAccomplishedOpexPont25;
	
	@Column(name = "USD_ACCOMPLISHED_OPEX_PONT_37")
	private Double usdAccomplishedOpexPont37;
	
	@Column(name = "USD_ACCOMPLISHED_OPEX_PONT_49")
	private Double usdAccomplishedOpexPont49;
	
	// Valores Mensais confirme Prazo Contratual
	@Column(name = "USD_ACCOMPLISHED_OPEX_MENS_12")
	private Double usdAccomplishedOpexMens12;
	
	@Column(name = "USD_ACCOMPLISHED_OPEX_MENS_24")
	private Double usdAccomplishedOpexMens24;
	
	@Column(name = "USD_ACCOMPLISHED_OPEX_MENS_36")
	private Double usdAccomplishedOpexMens36;
	
	@Column(name = "USD_ACCOMPLISHED_OPEX_MENS_48")
	private Double usdAccomplishedOpexMens48;

	@Column(name = "USD_ACCOMPLISHED_OPEX_MENS_60")
	private Double usdAccomplishedOpexMens60;
	
	// COGS
	// Valores Pontuais
	@Column(name = "USD_ACCOMPLISHED_COGS_INSTALL")
	private Double usdAccomplishedCogsInstall;
	
	@Column(name = "USD_ACCOMPLISHED_COGS_PONT_1")
	private Double usdAccomplishedCogsPont1;
	
	@Column(name = "USD_ACCOMPLISHED_COGS_PONT_13")
	private Double usdAccomplishedCogsPont13;
	
	@Column(name = "USD_ACCOMPLISHED_COGS_PONT_25")
	private Double usdAccomplishedCogsPont25;
	
	@Column(name = "USD_ACCOMPLISHED_COGS_PONT_37")
	private Double usdAccomplishedCogsPont37;
	
	@Column(name = "USD_ACCOMPLISHED_COGS_PONT_49")
	private Double usdAccomplishedCogsPont49;
	
	// Valores Mensais confirme Prazo Contratual
	@Column(name = "USD_ACCOMPLISHED_COGS_MENS_12")
	private Double usdAccomplishedCogsMens12;
	
	@Column(name = "USD_ACCOMPLISHED_COGS_MENS_24")
	private Double usdAccomplishedCogsMens24;
	
	@Column(name = "USD_ACCOMPLISHED_COGS_MENS_36")
	private Double usdAccomplishedCogsMens36;
	
	@Column(name = "USD_ACCOMPLISHED_COGS_MENS_48")
	private Double usdAccomplishedCogsMens48;

	@Column(name = "USD_ACCOMPLISHED_COGS_MENS_60")
	private Double usdAccomplishedCogsMens60;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created", unique = true, nullable = false)
	private Date created;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated", unique = true, nullable = false)
	private Date updated;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Package getPackage() {
		return pack;
	}

	public void setPackage(Package pack) {
		this.pack = pack;
	}

	public Double getBrlAllocatedCapexPont1() {
		return brlAllocatedCapexPont1;
	}

	public void setBrlAllocatedCapexPont1(Double brlAllocatedCapexPont1) {
		this.brlAllocatedCapexPont1 = brlAllocatedCapexPont1;
	}

	public Double getBrlAllocatedCapexPont13() {
		return brlAllocatedCapexPont13;
	}

	public void setBrlAllocatedCapexPont13(Double brlAllocatedCapexPont13) {
		this.brlAllocatedCapexPont13 = brlAllocatedCapexPont13;
	}

	public Double getBrlAllocatedCapexPont25() {
		return brlAllocatedCapexPont25;
	}

	public void setBrlAllocatedCapexPont25(Double brlAllocatedCapexPont25) {
		this.brlAllocatedCapexPont25 = brlAllocatedCapexPont25;
	}

	public Double getBrlAllocatedCapexPont37() {
		return brlAllocatedCapexPont37;
	}

	public void setBrlAllocatedCapexPont37(Double brlAllocatedCapexPont37) {
		this.brlAllocatedCapexPont37 = brlAllocatedCapexPont37;
	}

	public Double getBrlAllocatedCapexPont49() {
		return brlAllocatedCapexPont49;
	}

	public void setBrlAllocatedCapexPont49(Double brlAllocatedCapexPont49) {
		this.brlAllocatedCapexPont49 = brlAllocatedCapexPont49;
	}

	public Double getBrlAllocatedCapexMens12() {
		return brlAllocatedCapexMens12;
	}

	public void setBrlAllocatedCapexMens12(Double brlAllocatedCapexMens12) {
		this.brlAllocatedCapexMens12 = brlAllocatedCapexMens12;
	}

	public Double getBrlAllocatedCapexMens24() {
		return brlAllocatedCapexMens24;
	}

	public void setBrlAllocatedCapexMens24(Double brlAllocatedCapexMens24) {
		this.brlAllocatedCapexMens24 = brlAllocatedCapexMens24;
	}

	public Double getBrlAllocatedCapexMens36() {
		return brlAllocatedCapexMens36;
	}

	public void setBrlAllocatedCapexMens36(Double brlAllocatedCapexMens36) {
		this.brlAllocatedCapexMens36 = brlAllocatedCapexMens36;
	}

	public Double getBrlAllocatedCapexMens48() {
		return brlAllocatedCapexMens48;
	}

	public void setBrlAllocatedCapexMens48(Double brlAllocatedCapexMens48) {
		this.brlAllocatedCapexMens48 = brlAllocatedCapexMens48;
	}

	public Double getBrlAllocatedCapexMens60() {
		return brlAllocatedCapexMens60;
	}

	public void setBrlAllocatedCapexMens60(Double brlAllocatedCapexMens60) {
		this.brlAllocatedCapexMens60 = brlAllocatedCapexMens60;
	}

	public Double getBrlAllocatedOpexPont1() {
		return brlAllocatedOpexPont1;
	}

	public void setBrlAllocatedOpexPont1(Double brlAllocatedOpexPont1) {
		this.brlAllocatedOpexPont1 = brlAllocatedOpexPont1;
	}

	public Double getBrlAllocatedOpexPont13() {
		return brlAllocatedOpexPont13;
	}

	public void setBrlAllocatedOpexPont13(Double brlAllocatedOpexPont13) {
		this.brlAllocatedOpexPont13 = brlAllocatedOpexPont13;
	}

	public Double getBrlAllocatedOpexPont25() {
		return brlAllocatedOpexPont25;
	}

	public void setBrlAllocatedOpexPont25(Double brlAllocatedOpexPont25) {
		this.brlAllocatedOpexPont25 = brlAllocatedOpexPont25;
	}

	public Double getBrlAllocatedOpexPont37() {
		return brlAllocatedOpexPont37;
	}

	public void setBrlAllocatedOpexPont37(Double brlAllocatedOpexPont37) {
		this.brlAllocatedOpexPont37 = brlAllocatedOpexPont37;
	}

	public Double getBrlAllocatedOpexPont49() {
		return brlAllocatedOpexPont49;
	}

	public void setBrlAllocatedOpexPont49(Double brlAllocatedOpexPont49) {
		this.brlAllocatedOpexPont49 = brlAllocatedOpexPont49;
	}

	public Double getBrlAllocatedOpexMens12() {
		return brlAllocatedOpexMens12;
	}

	public void setBrlAllocatedOpexMens12(Double brlAllocatedOpexMens12) {
		this.brlAllocatedOpexMens12 = brlAllocatedOpexMens12;
	}

	public Double getBrlAllocatedOpexMens24() {
		return brlAllocatedOpexMens24;
	}

	public void setBrlAllocatedOpexMens24(Double brlAllocatedOpexMens24) {
		this.brlAllocatedOpexMens24 = brlAllocatedOpexMens24;
	}

	public Double getBrlAllocatedOpexMens36() {
		return brlAllocatedOpexMens36;
	}

	public void setBrlAllocatedOpexMens36(Double brlAllocatedOpexMens36) {
		this.brlAllocatedOpexMens36 = brlAllocatedOpexMens36;
	}

	public Double getBrlAllocatedOpexMens48() {
		return brlAllocatedOpexMens48;
	}

	public void setBrlAllocatedOpexMens48(Double brlAllocatedOpexMens48) {
		this.brlAllocatedOpexMens48 = brlAllocatedOpexMens48;
	}

	public Double getBrlAllocatedOpexMens60() {
		return brlAllocatedOpexMens60;
	}

	public void setBrlAllocatedOpexMens60(Double brlAllocatedOpexMens60) {
		this.brlAllocatedOpexMens60 = brlAllocatedOpexMens60;
	}

	public Double getBrlAllocatedCogsPont1() {
		return brlAllocatedCogsPont1;
	}

	public void setBrlAllocatedCogsPont1(Double brlAllocatedCogsPont1) {
		this.brlAllocatedCogsPont1 = brlAllocatedCogsPont1;
	}

	public Double getBrlAllocatedCogsPont13() {
		return brlAllocatedCogsPont13;
	}

	public void setBrlAllocatedCogsPont13(Double brlAllocatedCogsPont13) {
		this.brlAllocatedCogsPont13 = brlAllocatedCogsPont13;
	}

	public Double getBrlAllocatedCogsPont25() {
		return brlAllocatedCogsPont25;
	}

	public void setBrlAllocatedCogsPont25(Double brlAllocatedCogsPont25) {
		this.brlAllocatedCogsPont25 = brlAllocatedCogsPont25;
	}

	public Double getBrlAllocatedCogsPont37() {
		return brlAllocatedCogsPont37;
	}

	public void setBrlAllocatedCogsPont37(Double brlAllocatedCogsPont37) {
		this.brlAllocatedCogsPont37 = brlAllocatedCogsPont37;
	}

	public Double getBrlAllocatedCogsPont49() {
		return brlAllocatedCogsPont49;
	}

	public void setBrlAllocatedCogsPont49(Double brlAllocatedCogsPont49) {
		this.brlAllocatedCogsPont49 = brlAllocatedCogsPont49;
	}

	public Double getBrlAllocatedCogsMens12() {
		return brlAllocatedCogsMens12;
	}

	public void setBrlAllocatedCogsMens12(Double brlAllocatedCogsMens12) {
		this.brlAllocatedCogsMens12 = brlAllocatedCogsMens12;
	}

	public Double getBrlAllocatedCogsMens24() {
		return brlAllocatedCogsMens24;
	}

	public void setBrlAllocatedCogsMens24(Double brlAllocatedCogsMens24) {
		this.brlAllocatedCogsMens24 = brlAllocatedCogsMens24;
	}

	public Double getBrlAllocatedCogsMens36() {
		return brlAllocatedCogsMens36;
	}

	public void setBrlAllocatedCogsMens36(Double brlAllocatedCogsMens36) {
		this.brlAllocatedCogsMens36 = brlAllocatedCogsMens36;
	}

	public Double getBrlAllocatedCogsMens48() {
		return brlAllocatedCogsMens48;
	}

	public void setBrlAllocatedCogsMens48(Double brlAllocatedCogsMens48) {
		this.brlAllocatedCogsMens48 = brlAllocatedCogsMens48;
	}

	public Double getBrlAllocatedCogsMens60() {
		return brlAllocatedCogsMens60;
	}

	public void setBrlAllocatedCogsMens60(Double brlAllocatedCogsMens60) {
		this.brlAllocatedCogsMens60 = brlAllocatedCogsMens60;
	}

	public Double getBrlAccomplishedCapexPont1() {
		return brlAccomplishedCapexPont1;
	}

	public void setBrlAccomplishedCapexPont1(Double brlAccomplishedCapexPont1) {
		this.brlAccomplishedCapexPont1 = brlAccomplishedCapexPont1;
	}

	public Double getBrlAccomplishedCapexPont13() {
		return brlAccomplishedCapexPont13;
	}

	public void setBrlAccomplishedCapexPont13(Double brlAccomplishedCapexPont13) {
		this.brlAccomplishedCapexPont13 = brlAccomplishedCapexPont13;
	}

	public Double getBrlAccomplishedCapexPont25() {
		return brlAccomplishedCapexPont25;
	}

	public void setBrlAccomplishedCapexPont25(Double brlAccomplishedCapexPont25) {
		this.brlAccomplishedCapexPont25 = brlAccomplishedCapexPont25;
	}

	public Double getBrlAccomplishedCapexPont37() {
		return brlAccomplishedCapexPont37;
	}

	public void setBrlAccomplishedCapexPont37(Double brlAccomplishedCapexPont37) {
		this.brlAccomplishedCapexPont37 = brlAccomplishedCapexPont37;
	}

	public Double getBrlAccomplishedCapexPont49() {
		return brlAccomplishedCapexPont49;
	}

	public void setBrlAccomplishedCapexPont49(Double brlAccomplishedCapexPont49) {
		this.brlAccomplishedCapexPont49 = brlAccomplishedCapexPont49;
	}

	public Double getBrlAccomplishedCapexMens12() {
		return brlAccomplishedCapexMens12;
	}

	public void setBrlAccomplishedCapexMens12(Double brlAccomplishedCapexMens12) {
		this.brlAccomplishedCapexMens12 = brlAccomplishedCapexMens12;
	}

	public Double getBrlAccomplishedCapexMens24() {
		return brlAccomplishedCapexMens24;
	}

	public void setBrlAccomplishedCapexMens24(Double brlAccomplishedCapexMens24) {
		this.brlAccomplishedCapexMens24 = brlAccomplishedCapexMens24;
	}

	public Double getBrlAccomplishedCapexMens36() {
		return brlAccomplishedCapexMens36;
	}

	public void setBrlAccomplishedCapexMens36(Double brlAccomplishedCapexMens36) {
		this.brlAccomplishedCapexMens36 = brlAccomplishedCapexMens36;
	}

	public Double getBrlAccomplishedCapexMens48() {
		return brlAccomplishedCapexMens48;
	}

	public void setBrlAccomplishedCapexMens48(Double brlAccomplishedCapexMens48) {
		this.brlAccomplishedCapexMens48 = brlAccomplishedCapexMens48;
	}

	public Double getBrlAccomplishedCapexMens60() {
		return brlAccomplishedCapexMens60;
	}

	public void setBrlAccomplishedCapexMens60(Double brlAccomplishedCapexMens60) {
		this.brlAccomplishedCapexMens60 = brlAccomplishedCapexMens60;
	}

	public Double getBrlAccomplishedOpexPont1() {
		return brlAccomplishedOpexPont1;
	}

	public void setBrlAccomplishedOpexPont1(Double brlAccomplishedOpexPont1) {
		this.brlAccomplishedOpexPont1 = brlAccomplishedOpexPont1;
	}

	public Double getBrlAccomplishedOpexPont13() {
		return brlAccomplishedOpexPont13;
	}

	public void setBrlAccomplishedOpexPont13(Double brlAccomplishedOpexPont13) {
		this.brlAccomplishedOpexPont13 = brlAccomplishedOpexPont13;
	}

	public Double getBrlAccomplishedOpexPont25() {
		return brlAccomplishedOpexPont25;
	}

	public void setBrlAccomplishedOpexPont25(Double brlAccomplishedOpexPont25) {
		this.brlAccomplishedOpexPont25 = brlAccomplishedOpexPont25;
	}

	public Double getBrlAccomplishedOpexPont37() {
		return brlAccomplishedOpexPont37;
	}

	public void setBrlAccomplishedOpexPont37(Double brlAccomplishedOpexPont37) {
		this.brlAccomplishedOpexPont37 = brlAccomplishedOpexPont37;
	}

	public Double getBrlAccomplishedOpexPont49() {
		return brlAccomplishedOpexPont49;
	}

	public void setBrlAccomplishedOpexPont49(Double brlAccomplishedOpexPont49) {
		this.brlAccomplishedOpexPont49 = brlAccomplishedOpexPont49;
	}

	public Double getBrlAccomplishedOpexMens12() {
		return brlAccomplishedOpexMens12;
	}

	public void setBrlAccomplishedOpexMens12(Double brlAccomplishedOpexMens12) {
		this.brlAccomplishedOpexMens12 = brlAccomplishedOpexMens12;
	}

	public Double getBrlAccomplishedOpexMens24() {
		return brlAccomplishedOpexMens24;
	}

	public void setBrlAccomplishedOpexMens24(Double brlAccomplishedOpexMens24) {
		this.brlAccomplishedOpexMens24 = brlAccomplishedOpexMens24;
	}

	public Double getBrlAccomplishedOpexMens36() {
		return brlAccomplishedOpexMens36;
	}

	public void setBrlAccomplishedOpexMens36(Double brlAccomplishedOpexMens36) {
		this.brlAccomplishedOpexMens36 = brlAccomplishedOpexMens36;
	}

	public Double getBrlAccomplishedOpexMens48() {
		return brlAccomplishedOpexMens48;
	}

	public void setBrlAccomplishedOpexMens48(Double brlAccomplishedOpexMens48) {
		this.brlAccomplishedOpexMens48 = brlAccomplishedOpexMens48;
	}

	public Double getBrlAccomplishedOpexMens60() {
		return brlAccomplishedOpexMens60;
	}

	public void setBrlAccomplishedOpexMens60(Double brlAccomplishedOpexMens60) {
		this.brlAccomplishedOpexMens60 = brlAccomplishedOpexMens60;
	}

	public Double getBrlAccomplishedCogsPont1() {
		return brlAccomplishedCogsPont1;
	}

	public void setBrlAccomplishedCogsPont1(Double brlAccomplishedCogsPont1) {
		this.brlAccomplishedCogsPont1 = brlAccomplishedCogsPont1;
	}

	public Double getBrlAccomplishedCogsPont13() {
		return brlAccomplishedCogsPont13;
	}

	public void setBrlAccomplishedCogsPont13(Double brlAccomplishedCogsPont13) {
		this.brlAccomplishedCogsPont13 = brlAccomplishedCogsPont13;
	}

	public Double getBrlAccomplishedCogsPont25() {
		return brlAccomplishedCogsPont25;
	}

	public void setBrlAccomplishedCogsPont25(Double brlAccomplishedCogsPont25) {
		this.brlAccomplishedCogsPont25 = brlAccomplishedCogsPont25;
	}

	public Double getBrlAccomplishedCogsPont37() {
		return brlAccomplishedCogsPont37;
	}

	public void setBrlAccomplishedCogsPont37(Double brlAccomplishedCogsPont37) {
		this.brlAccomplishedCogsPont37 = brlAccomplishedCogsPont37;
	}

	public Double getBrlAccomplishedCogsPont49() {
		return brlAccomplishedCogsPont49;
	}

	public void setBrlAccomplishedCogsPont49(Double brlAccomplishedCogsPont49) {
		this.brlAccomplishedCogsPont49 = brlAccomplishedCogsPont49;
	}

	public Double getBrlAccomplishedCogsMens12() {
		return brlAccomplishedCogsMens12;
	}

	public void setBrlAccomplishedCogsMens12(Double brlAccomplishedCogsMens12) {
		this.brlAccomplishedCogsMens12 = brlAccomplishedCogsMens12;
	}

	public Double getBrlAccomplishedCogsMens24() {
		return brlAccomplishedCogsMens24;
	}

	public void setBrlAccomplishedCogsMens24(Double brlAccomplishedCogsMens24) {
		this.brlAccomplishedCogsMens24 = brlAccomplishedCogsMens24;
	}

	public Double getBrlAccomplishedCogsMens36() {
		return brlAccomplishedCogsMens36;
	}

	public void setBrlAccomplishedCogsMens36(Double brlAccomplishedCogsMens36) {
		this.brlAccomplishedCogsMens36 = brlAccomplishedCogsMens36;
	}

	public Double getBrlAccomplishedCogsMens48() {
		return brlAccomplishedCogsMens48;
	}

	public void setBrlAccomplishedCogsMens48(Double brlAccomplishedCogsMens48) {
		this.brlAccomplishedCogsMens48 = brlAccomplishedCogsMens48;
	}

	public Double getBrlAccomplishedCogsMens60() {
		return brlAccomplishedCogsMens60;
	}

	public void setBrlAccomplishedCogsMens60(Double brlAccomplishedCogsMens60) {
		this.brlAccomplishedCogsMens60 = brlAccomplishedCogsMens60;
	}

	public Double getUsdAllocatedCapexPont1() {
		return usdAllocatedCapexPont1;
	}

	public void setUsdAllocatedCapexPont1(Double usdAllocatedCapexPont1) {
		this.usdAllocatedCapexPont1 = usdAllocatedCapexPont1;
	}

	public Double getUsdAllocatedCapexPont13() {
		return usdAllocatedCapexPont13;
	}

	public void setUsdAllocatedCapexPont13(Double usdAllocatedCapexPont13) {
		this.usdAllocatedCapexPont13 = usdAllocatedCapexPont13;
	}

	public Double getUsdAllocatedCapexPont25() {
		return usdAllocatedCapexPont25;
	}

	public void setUsdAllocatedCapexPont25(Double usdAllocatedCapexPont25) {
		this.usdAllocatedCapexPont25 = usdAllocatedCapexPont25;
	}

	public Double getUsdAllocatedCapexPont37() {
		return usdAllocatedCapexPont37;
	}

	public void setUsdAllocatedCapexPont37(Double usdAllocatedCapexPont37) {
		this.usdAllocatedCapexPont37 = usdAllocatedCapexPont37;
	}

	public Double getUsdAllocatedCapexPont49() {
		return usdAllocatedCapexPont49;
	}

	public void setUsdAllocatedCapexPont49(Double usdAllocatedCapexPont49) {
		this.usdAllocatedCapexPont49 = usdAllocatedCapexPont49;
	}

	public Double getUsdAllocatedCapexMens12() {
		return usdAllocatedCapexMens12;
	}

	public void setUsdAllocatedCapexMens12(Double usdAllocatedCapexMens12) {
		this.usdAllocatedCapexMens12 = usdAllocatedCapexMens12;
	}

	public Double getUsdAllocatedCapexMens24() {
		return usdAllocatedCapexMens24;
	}

	public void setUsdAllocatedCapexMens24(Double usdAllocatedCapexMens24) {
		this.usdAllocatedCapexMens24 = usdAllocatedCapexMens24;
	}

	public Double getUsdAllocatedCapexMens36() {
		return usdAllocatedCapexMens36;
	}

	public void setUsdAllocatedCapexMens36(Double usdAllocatedCapexMens36) {
		this.usdAllocatedCapexMens36 = usdAllocatedCapexMens36;
	}

	public Double getUsdAllocatedCapexMens48() {
		return usdAllocatedCapexMens48;
	}

	public void setUsdAllocatedCapexMens48(Double usdAllocatedCapexMens48) {
		this.usdAllocatedCapexMens48 = usdAllocatedCapexMens48;
	}

	public Double getUsdAllocatedCapexMens60() {
		return usdAllocatedCapexMens60;
	}

	public void setUsdAllocatedCapexMens60(Double usdAllocatedCapexMens60) {
		this.usdAllocatedCapexMens60 = usdAllocatedCapexMens60;
	}

	public Double getUsdAllocatedOpexPont1() {
		return usdAllocatedOpexPont1;
	}

	public void setUsdAllocatedOpexPont1(Double usdAllocatedOpexPont1) {
		this.usdAllocatedOpexPont1 = usdAllocatedOpexPont1;
	}

	public Double getUsdAllocatedOpexPont13() {
		return usdAllocatedOpexPont13;
	}

	public void setUsdAllocatedOpexPont13(Double usdAllocatedOpexPont13) {
		this.usdAllocatedOpexPont13 = usdAllocatedOpexPont13;
	}

	public Double getUsdAllocatedOpexPont25() {
		return usdAllocatedOpexPont25;
	}

	public void setUsdAllocatedOpexPont25(Double usdAllocatedOpexPont25) {
		this.usdAllocatedOpexPont25 = usdAllocatedOpexPont25;
	}

	public Double getUsdAllocatedOpexPont37() {
		return usdAllocatedOpexPont37;
	}

	public void setUsdAllocatedOpexPont37(Double usdAllocatedOpexPont37) {
		this.usdAllocatedOpexPont37 = usdAllocatedOpexPont37;
	}

	public Double getUsdAllocatedOpexPont49() {
		return usdAllocatedOpexPont49;
	}

	public void setUsdAllocatedOpexPont49(Double usdAllocatedOpexPont49) {
		this.usdAllocatedOpexPont49 = usdAllocatedOpexPont49;
	}

	public Double getUsdAllocatedOpexMens12() {
		return usdAllocatedOpexMens12;
	}

	public void setUsdAllocatedOpexMens12(Double usdAllocatedOpexMens12) {
		this.usdAllocatedOpexMens12 = usdAllocatedOpexMens12;
	}

	public Double getUsdAllocatedOpexMens24() {
		return usdAllocatedOpexMens24;
	}

	public void setUsdAllocatedOpexMens24(Double usdAllocatedOpexMens24) {
		this.usdAllocatedOpexMens24 = usdAllocatedOpexMens24;
	}

	public Double getUsdAllocatedOpexMens36() {
		return usdAllocatedOpexMens36;
	}

	public void setUsdAllocatedOpexMens36(Double usdAllocatedOpexMens36) {
		this.usdAllocatedOpexMens36 = usdAllocatedOpexMens36;
	}

	public Double getUsdAllocatedOpexMens48() {
		return usdAllocatedOpexMens48;
	}

	public void setUsdAllocatedOpexMens48(Double usdAllocatedOpexMens48) {
		this.usdAllocatedOpexMens48 = usdAllocatedOpexMens48;
	}

	public Double getUsdAllocatedOpexMens60() {
		return usdAllocatedOpexMens60;
	}

	public void setUsdAllocatedOpexMens60(Double usdAllocatedOpexMens60) {
		this.usdAllocatedOpexMens60 = usdAllocatedOpexMens60;
	}

	public Double getUsdAllocatedCogsPont1() {
		return usdAllocatedCogsPont1;
	}

	public void setUsdAllocatedCogsPont1(Double usdAllocatedCogsPont1) {
		this.usdAllocatedCogsPont1 = usdAllocatedCogsPont1;
	}

	public Double getUsdAllocatedCogsPont13() {
		return usdAllocatedCogsPont13;
	}

	public void setUsdAllocatedCogsPont13(Double usdAllocatedCogsPont13) {
		this.usdAllocatedCogsPont13 = usdAllocatedCogsPont13;
	}

	public Double getUsdAllocatedCogsPont25() {
		return usdAllocatedCogsPont25;
	}

	public void setUsdAllocatedCogsPont25(Double usdAllocatedCogsPont25) {
		this.usdAllocatedCogsPont25 = usdAllocatedCogsPont25;
	}

	public Double getUsdAllocatedCogsPont37() {
		return usdAllocatedCogsPont37;
	}

	public void setUsdAllocatedCogsPont37(Double usdAllocatedCogsPont37) {
		this.usdAllocatedCogsPont37 = usdAllocatedCogsPont37;
	}

	public Double getUsdAllocatedCogsPont49() {
		return usdAllocatedCogsPont49;
	}

	public void setUsdAllocatedCogsPont49(Double usdAllocatedCogsPont49) {
		this.usdAllocatedCogsPont49 = usdAllocatedCogsPont49;
	}

	public Double getUsdAllocatedCogsMens12() {
		return usdAllocatedCogsMens12;
	}

	public void setUsdAllocatedCogsMens12(Double usdAllocatedCogsMens12) {
		this.usdAllocatedCogsMens12 = usdAllocatedCogsMens12;
	}

	public Double getUsdAllocatedCogsMens24() {
		return usdAllocatedCogsMens24;
	}

	public void setUsdAllocatedCogsMens24(Double usdAllocatedCogsMens24) {
		this.usdAllocatedCogsMens24 = usdAllocatedCogsMens24;
	}

	public Double getUsdAllocatedCogsMens36() {
		return usdAllocatedCogsMens36;
	}

	public void setUsdAllocatedCogsMens36(Double usdAllocatedCogsMens36) {
		this.usdAllocatedCogsMens36 = usdAllocatedCogsMens36;
	}

	public Double getUsdAllocatedCogsMens48() {
		return usdAllocatedCogsMens48;
	}

	public void setUsdAllocatedCogsMens48(Double usdAllocatedCogsMens48) {
		this.usdAllocatedCogsMens48 = usdAllocatedCogsMens48;
	}

	public Double getUsdAllocatedCogsMens60() {
		return usdAllocatedCogsMens60;
	}

	public void setUsdAllocatedCogsMens60(Double usdAllocatedCogsMens60) {
		this.usdAllocatedCogsMens60 = usdAllocatedCogsMens60;
	}

	public Double getUsdAccomplishedCapexPont1() {
		return usdAccomplishedCapexPont1;
	}

	public void setUsdAccomplishedCapexPont1(Double usdAccomplishedCapexPont1) {
		this.usdAccomplishedCapexPont1 = usdAccomplishedCapexPont1;
	}

	public Double getUsdAccomplishedCapexPont13() {
		return usdAccomplishedCapexPont13;
	}

	public void setUsdAccomplishedCapexPont13(Double usdAccomplishedCapexPont13) {
		this.usdAccomplishedCapexPont13 = usdAccomplishedCapexPont13;
	}

	public Double getUsdAccomplishedCapexPont25() {
		return usdAccomplishedCapexPont25;
	}

	public void setUsdAccomplishedCapexPont25(Double usdAccomplishedCapexPont25) {
		this.usdAccomplishedCapexPont25 = usdAccomplishedCapexPont25;
	}

	public Double getUsdAccomplishedCapexPont37() {
		return usdAccomplishedCapexPont37;
	}

	public void setUsdAccomplishedCapexPont37(Double usdAccomplishedCapexPont37) {
		this.usdAccomplishedCapexPont37 = usdAccomplishedCapexPont37;
	}

	public Double getUsdAccomplishedCapexPont49() {
		return usdAccomplishedCapexPont49;
	}

	public void setUsdAccomplishedCapexPont49(Double usdAccomplishedCapexPont49) {
		this.usdAccomplishedCapexPont49 = usdAccomplishedCapexPont49;
	}

	public Double getUsdAccomplishedCapexMens12() {
		return usdAccomplishedCapexMens12;
	}

	public void setUsdAccomplishedCapexMens12(Double usdAccomplishedCapexMens12) {
		this.usdAccomplishedCapexMens12 = usdAccomplishedCapexMens12;
	}

	public Double getUsdAccomplishedCapexMens24() {
		return usdAccomplishedCapexMens24;
	}

	public void setUsdAccomplishedCapexMens24(Double usdAccomplishedCapexMens24) {
		this.usdAccomplishedCapexMens24 = usdAccomplishedCapexMens24;
	}

	public Double getUsdAccomplishedCapexMens36() {
		return usdAccomplishedCapexMens36;
	}

	public void setUsdAccomplishedCapexMens36(Double usdAccomplishedCapexMens36) {
		this.usdAccomplishedCapexMens36 = usdAccomplishedCapexMens36;
	}

	public Double getUsdAccomplishedCapexMens48() {
		return usdAccomplishedCapexMens48;
	}

	public void setUsdAccomplishedCapexMens48(Double usdAccomplishedCapexMens48) {
		this.usdAccomplishedCapexMens48 = usdAccomplishedCapexMens48;
	}

	public Double getUsdAccomplishedCapexMens60() {
		return usdAccomplishedCapexMens60;
	}

	public void setUsdAccomplishedCapexMens60(Double usdAccomplishedCapexMens60) {
		this.usdAccomplishedCapexMens60 = usdAccomplishedCapexMens60;
	}

	public Double getUsdAccomplishedOpexPont1() {
		return usdAccomplishedOpexPont1;
	}

	public void setUsdAccomplishedOpexPont1(Double usdAccomplishedOpexPont1) {
		this.usdAccomplishedOpexPont1 = usdAccomplishedOpexPont1;
	}

	public Double getUsdAccomplishedOpexPont13() {
		return usdAccomplishedOpexPont13;
	}

	public void setUsdAccomplishedOpexPont13(Double usdAccomplishedOpexPont13) {
		this.usdAccomplishedOpexPont13 = usdAccomplishedOpexPont13;
	}

	public Double getUsdAccomplishedOpexPont25() {
		return usdAccomplishedOpexPont25;
	}

	public void setUsdAccomplishedOpexPont25(Double usdAccomplishedOpexPont25) {
		this.usdAccomplishedOpexPont25 = usdAccomplishedOpexPont25;
	}

	public Double getUsdAccomplishedOpexPont37() {
		return usdAccomplishedOpexPont37;
	}

	public void setUsdAccomplishedOpexPont37(Double usdAccomplishedOpexPont37) {
		this.usdAccomplishedOpexPont37 = usdAccomplishedOpexPont37;
	}

	public Double getUsdAccomplishedOpexPont49() {
		return usdAccomplishedOpexPont49;
	}

	public void setUsdAccomplishedOpexPont49(Double usdAccomplishedOpexPont49) {
		this.usdAccomplishedOpexPont49 = usdAccomplishedOpexPont49;
	}

	public Double getUsdAccomplishedOpexMens12() {
		return usdAccomplishedOpexMens12;
	}

	public void setUsdAccomplishedOpexMens12(Double usdAccomplishedOpexMens12) {
		this.usdAccomplishedOpexMens12 = usdAccomplishedOpexMens12;
	}

	public Double getUsdAccomplishedOpexMens24() {
		return usdAccomplishedOpexMens24;
	}

	public void setUsdAccomplishedOpexMens24(Double usdAccomplishedOpexMens24) {
		this.usdAccomplishedOpexMens24 = usdAccomplishedOpexMens24;
	}

	public Double getUsdAccomplishedOpexMens36() {
		return usdAccomplishedOpexMens36;
	}

	public void setUsdAccomplishedOpexMens36(Double usdAccomplishedOpexMens36) {
		this.usdAccomplishedOpexMens36 = usdAccomplishedOpexMens36;
	}

	public Double getUsdAccomplishedOpexMens48() {
		return usdAccomplishedOpexMens48;
	}

	public void setUsdAccomplishedOpexMens48(Double usdAccomplishedOpexMens48) {
		this.usdAccomplishedOpexMens48 = usdAccomplishedOpexMens48;
	}

	public Double getUsdAccomplishedOpexMens60() {
		return usdAccomplishedOpexMens60;
	}

	public void setUsdAccomplishedOpexMens60(Double usdAccomplishedOpexMens60) {
		this.usdAccomplishedOpexMens60 = usdAccomplishedOpexMens60;
	}

	public Double getUsdAccomplishedCogsPont1() {
		return usdAccomplishedCogsPont1;
	}

	public void setUsdAccomplishedCogsPont1(Double usdAccomplishedCogsPont1) {
		this.usdAccomplishedCogsPont1 = usdAccomplishedCogsPont1;
	}

	public Double getUsdAccomplishedCogsPont13() {
		return usdAccomplishedCogsPont13;
	}

	public void setUsdAccomplishedCogsPont13(Double usdAccomplishedCogsPont13) {
		this.usdAccomplishedCogsPont13 = usdAccomplishedCogsPont13;
	}

	public Double getUsdAccomplishedCogsPont25() {
		return usdAccomplishedCogsPont25;
	}

	public void setUsdAccomplishedCogsPont25(Double usdAccomplishedCogsPont25) {
		this.usdAccomplishedCogsPont25 = usdAccomplishedCogsPont25;
	}

	public Double getUsdAccomplishedCogsPont37() {
		return usdAccomplishedCogsPont37;
	}

	public void setUsdAccomplishedCogsPont37(Double usdAccomplishedCogsPont37) {
		this.usdAccomplishedCogsPont37 = usdAccomplishedCogsPont37;
	}

	public Double getUsdAccomplishedCogsPont49() {
		return usdAccomplishedCogsPont49;
	}

	public void setUsdAccomplishedCogsPont49(Double usdAccomplishedCogsPont49) {
		this.usdAccomplishedCogsPont49 = usdAccomplishedCogsPont49;
	}

	public Double getUsdAccomplishedCogsMens12() {
		return usdAccomplishedCogsMens12;
	}

	public void setUsdAccomplishedCogsMens12(Double usdAccomplishedCogsMens12) {
		this.usdAccomplishedCogsMens12 = usdAccomplishedCogsMens12;
	}

	public Double getUsdAccomplishedCogsMens24() {
		return usdAccomplishedCogsMens24;
	}

	public void setUsdAccomplishedCogsMens24(Double usdAccomplishedCogsMens24) {
		this.usdAccomplishedCogsMens24 = usdAccomplishedCogsMens24;
	}

	public Double getUsdAccomplishedCogsMens36() {
		return usdAccomplishedCogsMens36;
	}

	public void setUsdAccomplishedCogsMens36(Double usdAccomplishedCogsMens36) {
		this.usdAccomplishedCogsMens36 = usdAccomplishedCogsMens36;
	}

	public Double getUsdAccomplishedCogsMens48() {
		return usdAccomplishedCogsMens48;
	}

	public void setUsdAccomplishedCogsMens48(Double usdAccomplishedCogsMens48) {
		this.usdAccomplishedCogsMens48 = usdAccomplishedCogsMens48;
	}

	public Double getUsdAccomplishedCogsMens60() {
		return usdAccomplishedCogsMens60;
	}

	public void setUsdAccomplishedCogsMens60(Double usdAccomplishedCogsMens60) {
		this.usdAccomplishedCogsMens60 = usdAccomplishedCogsMens60;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Double getBrlAllocatedCapexInstall() {
		return brlAllocatedCapexInstall;
	}

	public void setBrlAllocatedCapexInstall(Double brlAllocatedCapexInstall) {
		this.brlAllocatedCapexInstall = brlAllocatedCapexInstall;
	}

	public Double getBrlAllocatedOpexInstall() {
		return brlAllocatedOpexInstall;
	}

	public void setBrlAllocatedOpexInstall(Double brlAllocatedOpexInstall) {
		this.brlAllocatedOpexInstall = brlAllocatedOpexInstall;
	}

	public Double getBrlAllocatedCogsInstall() {
		return brlAllocatedCogsInstall;
	}

	public void setBrlAllocatedCogsInstall(Double brlAllocatedCogsInstall) {
		this.brlAllocatedCogsInstall = brlAllocatedCogsInstall;
	}

	public Double getBrlAccomplishedCapexInstall() {
		return brlAccomplishedCapexInstall;
	}

	public void setBrlAccomplishedCapexInstall(Double brlAccomplishedCapexInstall) {
		this.brlAccomplishedCapexInstall = brlAccomplishedCapexInstall;
	}

	public Double getBrlAccomplishedOpexInstall() {
		return brlAccomplishedOpexInstall;
	}

	public void setBrlAccomplishedOpexInstall(Double brlAccomplishedOpexInstall) {
		this.brlAccomplishedOpexInstall = brlAccomplishedOpexInstall;
	}

	public Double getBrlAccomplishedCogsInstall() {
		return brlAccomplishedCogsInstall;
	}

	public void setBrlAccomplishedCogsInstall(Double brlAccomplishedCogsInstall) {
		this.brlAccomplishedCogsInstall = brlAccomplishedCogsInstall;
	}

	public Double getUsdAllocatedCapexInstall() {
		return usdAllocatedCapexInstall;
	}

	public void setUsdAllocatedCapexInstall(Double usdAllocatedCapexInstall) {
		this.usdAllocatedCapexInstall = usdAllocatedCapexInstall;
	}

	public Double getUsdAllocatedOpexInstall() {
		return usdAllocatedOpexInstall;
	}

	public void setUsdAllocatedOpexInstall(Double usdAllocatedOpexInstall) {
		this.usdAllocatedOpexInstall = usdAllocatedOpexInstall;
	}

	public Double getUsdAllocatedCogsInstall() {
		return usdAllocatedCogsInstall;
	}

	public void setUsdAllocatedCogsInstall(Double usdAllocatedCogsInstall) {
		this.usdAllocatedCogsInstall = usdAllocatedCogsInstall;
	}

	public Double getUsdAccomplishedCapexInstall() {
		return usdAccomplishedCapexInstall;
	}

	public void setUsdAccomplishedCapexInstall(Double usdAccomplishedCapexInstall) {
		this.usdAccomplishedCapexInstall = usdAccomplishedCapexInstall;
	}

	public Double getUsdAccomplishedOpexInstall() {
		return usdAccomplishedOpexInstall;
	}

	public void setUsdAccomplishedOpexInstall(Double usdAccomplishedOpexInstall) {
		this.usdAccomplishedOpexInstall = usdAccomplishedOpexInstall;
	}

	public Double getUsdAccomplishedCogsInstall() {
		return usdAccomplishedCogsInstall;
	}

	public void setUsdAccomplishedCogsInstall(Double usdAccomplishedCogsInstall) {
		this.usdAccomplishedCogsInstall = usdAccomplishedCogsInstall;
	}

	
}
