package br.com.concil.product.matrix;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.concil.product.matrix.repository.Matrix;
import br.com.concil.product.matrix.repository.MatrixRepository;

@Service
public class MatrixService {

	@Autowired
	private MatrixRepository repository;
	
	public Matrix findById(Long id) {
		return repository.findOne(id);
	}
	
	public List<Matrix> findAll() {
		return repository.findAll();
	}
	
	public Matrix save(Matrix matrix) {
		matrix.setCreated(new Date());
		return repository.save(matrix);
	}
	
	public Matrix update(Long id, Matrix matrix) {
		matrix.setUpdated(new Date());
		return repository.saveAndFlush(matrix);
	}
	
	public void delete(Long id) {
		repository.delete(id);
	}
	
	public void deleteAll() {
		repository.deleteAll();
	}
	
}
