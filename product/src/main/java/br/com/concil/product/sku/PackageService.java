package br.com.concil.product.sku;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.concil.product.sku.repository.Package;
import br.com.concil.product.sku.repository.PackageRepository;

@Service
public class PackageService {

	@Autowired
	private PackageRepository repository;
	
	public Package findById(Long id) {
		return repository.findOne(id);
	}
	
	public List<Package> findAll() {
		return repository.findAll();
	}
	
	public Package save(Package packages) {
		packages.setCreated(new Date());
		return repository.save(packages);
	}
	
	public Package update(Long id, Package packages) {
		packages.setUpdated(new Date());
		return repository.saveAndFlush(packages);
	}
	
	public void delete(Long id) {
		repository.delete(id);
	}
	
	public void deleteAll() {
		repository.deleteAll();
	}
	
}
