package br.com.concil.product.tax;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.concil.product.tax.repository.Tax;
import br.com.concil.product.tax.repository.TaxRepository;

@Service
public class TaxService {

	@Autowired
	private TaxRepository repository;
	
	public Tax findById(Long id) {
		return repository.findOne(id);
	}
	
	public List<Tax> findAll() {
		return repository.findAll();
	}
	
	public Tax save(Tax tax) {
		tax.setCreated(new Date());
		return repository.save(tax);
	}
	
	public Tax update(Long id, Tax tax) {
		tax.setUpdated(new Date());
		return repository.saveAndFlush(tax);
	}
	
	public void delete(Long id) {
		repository.delete(id);
	}
	
	public void deleteAll() {
		repository.deleteAll();
	}
	
}
