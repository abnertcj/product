package br.com.concil.product.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.concil.product.tax.TaxService;
import br.com.concil.product.tax.repository.Tax;

@RestController
public class TaxController {
	
	@Autowired
	private TaxService service;
	
	@RequestMapping(value = "/api/tax/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Tax> findById(@PathVariable("id") Long id) {
		Tax tax = service.findById(id);
		if (tax == null)
			return new ResponseEntity<Tax>(tax, HttpStatus.NOT_FOUND);
		return new ResponseEntity<Tax>(tax, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/tax", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Tax>> findAll() {
		List<Tax> taxes = service.findAll();
		if (taxes.isEmpty())
			return new ResponseEntity<List<Tax>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Tax>>(taxes, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/tax", method = RequestMethod.POST)
	public ResponseEntity<Tax> save(@RequestBody Tax tax) {
		Tax taxSaved = service.save(tax);
		return new ResponseEntity<Tax>(taxSaved, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/tax/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Tax> update(@PathVariable Long id, @RequestBody Tax tax) {
		Tax taxUpdated = service.update(id, tax);
		return new ResponseEntity<Tax>(taxUpdated, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/tax/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Tax> delete(@PathVariable("id") Long id) {
		service.delete(id);
		return new ResponseEntity<Tax>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/tax", method = RequestMethod.DELETE)
	public ResponseEntity<Tax> deleteAll() {
		service.deleteAll();
		return new ResponseEntity<Tax>(HttpStatus.OK);
	}

}
