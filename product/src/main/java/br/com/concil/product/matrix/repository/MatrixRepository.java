package br.com.concil.product.matrix.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MatrixRepository extends JpaRepository<Matrix, Long>, JpaSpecificationExecutor<Matrix> {

}
