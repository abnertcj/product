package br.com.concil.product.sku.repository;

import java.io.Serializable;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class PackageSpecification<T> implements Specification<T>, Serializable {

	private static final long serialVersionUID = 6377373013811072321L;
	
	private String company;

//	public CompanySpecification(ParameterModel parametros) {
//		String company = parametros.getEmpresa();
//		if (StringUtils.isNotEmpty(company))
//			this.company = company;
//		else
//			this.company = null;
//	}
	
	@Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		if (this.company == null) {
			return null;
		} else {
			Expression<String> expression = root.get("companyCd");
			expression = cb.upper(expression);
			return expression.in(this.company.toUpperCase());
		}
	}

}
