package br.com.concil.product.sku.repository;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

public class StandardPackageSpecificationBuilder {

	public Specification<Package> build(Object parameter) {
		return Specifications.where(new PackageSpecification<Package>());
//		return Specifications.where(new CompanySpecification<Product>(parameter)
//				.and(new AcquirerSpecification<SalesOrder>(parameter));
	}
}
