package br.com.concil.product.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.concil.product.group.GroupService;
import br.com.concil.product.group.repository.Group;
import br.com.concil.product.product.repository.Software;

@Controller
public class GroupController {

	@Autowired
	private GroupService service;

	@RequestMapping(value = "/api/group/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Group> findById(@PathVariable("id") Long id) {
		Group group = service.findById(id);
		if (group == null)
			return new ResponseEntity<Group>(group, HttpStatus.NOT_FOUND);
		return new ResponseEntity<Group>(group, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/group", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Group>> findAll() {
		List<Group> groups = service.findAll();
		if (groups.isEmpty())
			return new ResponseEntity<List<Group>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Group>>(groups, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/group", method = RequestMethod.POST)
	public ResponseEntity<Group> save(@RequestBody Group group) {
		List<Software> softwares = group.getSoftwares();
		group.setSoftwares(null);
		Group groupSavedWithoutSoftwares = service.save(group);
		
		groupSavedWithoutSoftwares.setSoftwares(softwares);
		Group groupSaved = service.save(groupSavedWithoutSoftwares);
		
		return new ResponseEntity<Group>(groupSaved, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/group/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Group> update(@PathVariable Long id, @RequestBody Group group) {
		List<Software> softwares = group.getSoftwares();
		group.setSoftwares(null);
		
		Group groupUpdatedWithoutSoftwares = service.update(id, group);
		
		groupUpdatedWithoutSoftwares.setSoftwares(softwares);
		Group groupUpdated = service.update(groupUpdatedWithoutSoftwares.getId(), groupUpdatedWithoutSoftwares);
		
		return new ResponseEntity<Group>(groupUpdated, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/group/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Group> delete(@PathVariable("id") Long id) {
		service.delete(id);
		return new ResponseEntity<Group>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/group", method = RequestMethod.DELETE)
	public ResponseEntity<Group> deleteAll() {
		service.deleteAll();
		return new ResponseEntity<Group>(HttpStatus.OK);
	}
}
