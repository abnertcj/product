package br.com.concil.product.addon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AddonRepository extends JpaRepository<Addon, Long>, JpaSpecificationExecutor<Addon> {

}
