package br.com.concil.product.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.concil.product.addon.AddonService;
import br.com.concil.product.addon.repository.Addon;
import br.com.concil.product.product.ProductService;
import br.com.concil.product.product.repository.Product;

@RestController
public class AddonController {
	
	@Autowired
	private AddonService service;
	
	@Autowired
	private ProductService productService;

	@RequestMapping(value = "/api/addon/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Addon> findById(@PathVariable("id") Long id) {
		Addon addon = service.findById(id);
		if (addon == null)
			return new ResponseEntity<Addon>(addon, HttpStatus.NOT_FOUND);
		return new ResponseEntity<Addon>(addon, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/addon", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Addon>> findAll() {
		List<Addon> addons = service.findAll();
		if (addons.isEmpty())
			return new ResponseEntity<List<Addon>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Addon>>(addons, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/addon", method = RequestMethod.POST)
	public ResponseEntity<Addon> save(@RequestBody Addon addon) {
		Product product = addon.getProduct();
		Product productSaved = productService.save(product);
		addon.setProduct(null);
		
		Addon addonSavedWithoutRelationships = service.save(addon);
		
		addonSavedWithoutRelationships.setProduct(productSaved);
		Addon addonSaved = service.update(addonSavedWithoutRelationships.getId(), addonSavedWithoutRelationships);
		
		return new ResponseEntity<Addon>(addonSaved, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/addon/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Addon> update(@PathVariable Long id, @RequestBody Addon addon) {
		Addon addonSaved = service.findById(id);
		Product productSaved = addonSaved.getProduct();
		if (productSaved != null) {
			addon.getProduct().setId(productSaved.getId());
			productService.update(productSaved.getId(), addon.getProduct());
		}
		
		Addon addonUpdated = service.update(id, addon);
		return new ResponseEntity<Addon>(addonUpdated, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/addon/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Addon> delete(@PathVariable("id") Long id) {
		Addon addon = service.findById(id);
		Product product = addon.getProduct();
		
		service.delete(id);
		productService.delete(product.getId());
		return new ResponseEntity<Addon>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/addon", method = RequestMethod.DELETE)
	public ResponseEntity<Addon> deleteAll() {
		service.deleteAll();
		return new ResponseEntity<Addon>(HttpStatus.OK);
	}
	
}