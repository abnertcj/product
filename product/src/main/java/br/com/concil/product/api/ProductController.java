package br.com.concil.product.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.concil.product.product.ProductService;
import br.com.concil.product.product.repository.Product;

@RestController
public class ProductController {
	
	@Autowired
	private ProductService service;

	@RequestMapping(value = "/api/product/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Product> findById(@PathVariable("id") Long id) {
		Product product = service.findById(id);
		if (product == null)
			return new ResponseEntity<Product>(product, HttpStatus.NOT_FOUND);
		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/product", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Product>> findAll() {
		List<Product> products = service.findAll();
		if (products.isEmpty())
			return new ResponseEntity<List<Product>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
	}
	
//	@RequestMapping(value = "/api/software", method = RequestMethod.POST)
//	public ResponseEntity<Software> save(@RequestBody Software software) {
//		List<Tax> taxes = software.getTaxes();
//		software.setTaxes(null);
//		
//		Product product = software.getProduct();
//		Product productSaved = productService.save(product);
//		software.setProduct(null);
//		
//		Software softwareSavedWithoutRelationships = service.save(software);
//		
//		softwareSavedWithoutRelationships.setTaxes(taxes);
//		softwareSavedWithoutRelationships.setProduct(productSaved);
//		Software softwareSaved = service.update(softwareSavedWithoutRelationships.getId(), softwareSavedWithoutRelationships);
//		
//		return new ResponseEntity<Software>(softwareSaved, HttpStatus.OK);
//	}
//	
//	@RequestMapping(value = "/api/software/{id}", method = RequestMethod.PUT)
//	public void update(@PathVariable Long id, @RequestBody Software software) {
//		List<Tax> taxes = software.getTaxes();
//		software.setTaxes(null);
//		
//		Software softwareSaved = service.findById(id);
//		Product productSaved = softwareSaved.getProduct();
//		if (productSaved != null) {
//			software.getProduct().setId(productSaved.getId());
//			productService.update(productSaved.getId(), software.getProduct());
//		}
//		
//		Software softwareUpdatedWithoutRelationships = service.update(id, software);
//		
//		softwareUpdatedWithoutRelationships.setTaxes(taxes);
//		Software softwareUpdated = service.update(softwareUpdatedWithoutRelationships.getId(), softwareUpdatedWithoutRelationships);
//		
//		// return new ResponseEntity<Software>(softwareUpdated, HttpStatus.OK);
//	}
//	
//	@RequestMapping(value = "/api/software/{id}", method = RequestMethod.DELETE)
//	public ResponseEntity<Software> delete(@PathVariable("id") Long id) {
//		Software software = service.findById(id);
//		Product product = software.getProduct();
//		
//		service.delete(id);
//		productService.delete(product.getId());
//		return new ResponseEntity<Software>(HttpStatus.OK);
//	}
//	
//	@RequestMapping(value = "/api/software", method = RequestMethod.DELETE)
//	public ResponseEntity<Software> deleteAll() {
//		service.deleteAll();
//		return new ResponseEntity<Software>(HttpStatus.OK);
//	}
	
}