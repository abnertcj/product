package br.com.concil.product.addon.repository;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.concil.product.product.repository.Product;

@Entity
@Table(name = "TB_ADDON")
public class Addon implements Serializable {
	
	private static final long serialVersionUID = -7212565678635547190L;

	@Id
	@Column(name = "ID")
	@GeneratedValue
	private Long id;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_PRODUCT")
	private Product product;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
