package br.com.concil.product.sku.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PackageRepository extends JpaRepository<Package, Long>, JpaSpecificationExecutor<Package> {
	
}
