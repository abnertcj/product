package br.com.concil.product.product;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.concil.product.product.repository.Software;
import br.com.concil.product.product.repository.SoftwareRepository;

@Service
public class SoftwareService {

	@Autowired
	private SoftwareRepository repository;
	
	public Software findById(Long id) {
		return repository.findOne(id);
	}
	
	public List<Software> findAll() {
//		StandardProductSpecificationBuilder spec = new StandardProductSpecificationBuilder();
//		return repository.findAll(spec.build(new Object()));
		return repository.findAll();
	}
	
	public Software save(Software software) {
		return repository.save(software);
	}
	
	public Software update(Long id, Software software) {
		return repository.saveAndFlush(software);
	}
	
	public void delete(Long id) {
		repository.delete(id);
	}
	
	public void deleteAll() {
		repository.deleteAll();
	}
	
}
