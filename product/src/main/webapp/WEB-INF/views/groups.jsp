<!DOCTYPE html>
<html ng-app="concilApp">
	<head>
		<script type="text/javascript" src="resources/javascript/angularjs/angular.js"></script>
		<script type="text/javascript" src="resources/javascript/angularjs/angular-route.js"></script>
		<script type="text/javascript" src="resources/javascript/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="resources/javascript/concil-app.js"></script>
		<script type="text/javascript" src="resources/javascript/app/ajax-service.js"></script>
		<script type="text/javascript" src="resources/javascript/app/groups/groups-controller.js"></script>
		<script type="text/javascript" src="./resources/bootstrap/dist/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="./resources/bootstrap/dist/css/bootstrap.min.css">
	</head>
	<body ng-controller="GroupsController as controller" ng-init="service.init()">
	
		<!-- Navigation Bar -->
		<nav class="navbar navbar-inverse navbar-fixed-top">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">Concil - Product</a>
		      <ul class="nav navbar-nav navbar-right">
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="index">Index</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="products">Produtos</a></li>
		            <li><a href="taxes">Taxas</a></li>
		            <li><a href="addons">Addons</a></li>
		            <li><a href="groups">Grupos</a></li>
		            <li><a href="packages">Pacotes</a></li>
		            <li><a href="matrix">Matriz de Custo</a></li>
		          </ul>
		        </li>
		      </ul>
		    </div>
		  </div>
		</nav>
		<!-- End Navigation Bar -->
		
		<div class="container">
		
			<br /><br />
			
			<h1>Grupos</h1>
			
			<div ng-show="model.isSuccess" class="alert alert-success" role="alert"><center>Opera��o realizada com sucesso!</center></div>
			<div ng-show="model.isFailure" class="alert alert-danger" role="alert"><center>Falha na opera��o!</center></div>
			
			<!-- Create -->
			<form class="navbar-form navbar-left" role="search" ng-show="model.action == 'create'">
				<div class="form-group">
					<label>Nome</label><br />
					<input type="text" ng-model="model.newGroup.name" class="form-control" name="name">
					<br /><br />
					<label>Produtos</label><br />
					<select ng-model="model.newGroup.softwares" class="form-control" multiple size="6">
						<option ng-repeat="software in softwares" value="{{software.id}}">{{software.product.name}}</option>
					</select>
					<br /><br />
					<button class="btn btn-success" ng-click="model.createGroup()">Cadastrar</button>
					<button class="btn btn-info" ng-click="model.action='view';">Cancelar</button>
				</div>
			</form>
			
			<!-- Update -->
			<form class="navbar-form navbar-left" role="search" ng-show="model.action == 'update'">
				<div class="form-group">
					<input type="hidden" ng-model="model.updatedGroup.id" name="id">
					<input type="hidden" ng-model="model.updatedGroup.created" name="created">
					<label>Nome</label><br />
					<input type="text" ng-model="model.updatedGroup.name" class="form-control" name="name"><br /><br />
					<label>Produtos</label><br />
					<select ng-model="model.updatedGroup.softwares" class="form-control" multiple size="6">
						<option ng-repeat="software in softwares" ng-if="model.isSelectedSoftware(software.id)" selected="selected" value="{{software.id}}">{{software.product.name}}</option>
						<option ng-repeat="software in softwares" ng-if="!model.isSelectedSoftware(software.id)" value="{{software.id}}">{{software.product.name}}</option>
<!-- 						<option ng-repeat="software in softwares" ng-selected="model.isSelectedSoftware(software.id)" value="{{software.id}}">{{software.name}}</option> -->
					</select>
					<br /><br />
					<button class="btn btn-primary" ng-click="model.confirmUpdateGroup()">Atualizar</button>
					<button class="btn btn-info" ng-click="model.action='view';">Cancelar</button>
				</div>
			</form>
			
			<!-- View -->
			<div ng-show="model.action == 'view'">
				<button class="btn btn-success" ng-click="model.openCreateGroup()">Novo</button>
				<br /><br />
				<div class="panel panel-default">
					<div class="panel-heading">Lista de Grupos</div>
					
					<table class="table">
						<tr><th>ID<th>Grupo<th>Editar<th>Excluir
						<tr ng-repeat="group in groups">
							<td>{{group.id}}
							<td>{{group.name}}
							<td><button class="btn btn-info" ng-click="model.updateGroup($index)">Editar</button>
							<td><button class="btn btn-danger" ng-click="model.deleteGroup($index);">Excluir</button>
						</tr>
					</table>
					
					<div class="panel-footer"><button class="btn btn-warning" ng-click="model.deleteGroups();">Excluir Tudo</button></div>
				</div>
			</div>
			
		</div>
	
	</body>
</html>