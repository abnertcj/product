<!DOCTYPE html>
<html ng-app="concilApp">
	<head>
		<script type="text/javascript" src="resources/javascript/angularjs/angular.js"></script>
		<script type="text/javascript" src="resources/javascript/angularjs/angular-route.js"></script>
		<script type="text/javascript" src="resources/javascript/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="resources/javascript/concil-app.js"></script>
		<script type="text/javascript" src="resources/javascript/app/ajax-service.js"></script>
		<script type="text/javascript" src="resources/javascript/app/matrix/matrix-controller.js"></script>
		<script type="text/javascript" src="./resources/bootstrap/dist/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="./resources/bootstrap/dist/css/bootstrap.min.css">
	</head>
	<body ng-controller="MatrixController as controller" ng-init="service.init()">
	
		<!-- Navigation Bar -->
		<nav class="navbar navbar-inverse navbar-fixed-top">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">Concil - Product</a>
		      <ul class="nav navbar-nav navbar-right">
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="index">Index</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="products">Produtos</a></li>
		            <li><a href="taxes">Taxas</a></li>
		            <li><a href="addons">Addons</a></li>
		            <li><a href="groups">Grupos</a></li>
		            <li><a href="packages">Pacotes</a></li>
		            <li><a href="matrix">Matriz de Custo</a></li>
		          </ul>
		        </li>
		      </ul>
		    </div>
		  </div>
		</nav>
		<!-- End Navigation Bar -->
		
		<div class="container">
		
			<br /><br />
		
			<h1>Matriz de Custo</h1>
			
			<div ng-show="model.isSuccess" class="alert alert-success" role="alert"><center>Opera��o realizada com sucesso!</center></div>
			<div ng-show="model.isFailure" class="alert alert-danger" role="alert"><center>Falha na opera��o!</center></div>
			
			<!-- Create -->
			<form class="navbar-form navbar-left" class="col-md-2 col-xs-2" role="form" ng-show="model.action == 'create'">
					
				<label>Nome</label><br />
				<input type="text" ng-model="model.newMatrix.name" style="width: 90px;" class="form-control" placeholder="" name="name"><br /><br />
			
				<label>Pacote</label><br />
				<select ng-model="model.newMatrix.package" class="form-control">
					<option ng-repeat="package in packages" value="{{package.id}}">{{package.name}}</option>
				</select>
				<br /><br />
			
				<table border="1">
					<tr>
						<td colspan="3" rowspan="2"></td>
						<td rowspan="2">Instala��o</td>
						<td colspan="5">Valores Pontuais</td>
						<td colspan="5">Valores Mensais conforme Prazo Contratual</td>
					</tr>
					<tr>
						<td>M�s 1</td>
						<td>M�s 13</td>
						<td>M�s 25</td>
						<td>M�s 37</td>
						<td>M�s 49</td>
						<td>12 Meses</td>
						<td>24 Meses</td>
						<td>36 Meses</td>
						<td>48 Meses</td>
						<td>60 Meses</td>
					</tr>
					
					<!-- BRL -->
					<!-- ALLOCATED -->
					<tr>
						<td rowspan="6" style="transform:rotate(-90deg);">BRL</td>
						<td rowspan="3" style="transform:rotate(-90deg);">ALOCADO</td>
						<td>CAPEX</td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCapexInstall" style="width: 90px;" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexInstall"></td>
						<!-- Valores Pontuais -->
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCapexPont1" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexPont1"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCapexPont13" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexPont13"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCapexPont25" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexPont25"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCapexPont37" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexPont37"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCapexPont49" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCapexMens12" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexMens12"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCapexMens24" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexMens24"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCapexMens36" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexMens36"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCapexMens48" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexMens48"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCapexMens60" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexMens60"></td>
					</tr>
					<tr>
						<td>OPEX</td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedOpexInstall" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexInstall"></td>
						<!-- Valores Pontuais -->
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedOpexPont1" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexPont1"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedOpexPont13" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexPont13"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedOpexPont25" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexPont25"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedOpexPont37" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexPont37"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedOpexPont49" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedOpexMens12" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexMens12"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedOpexMens24" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexMens24"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedOpexMens36" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexMens36"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedOpexMens48" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexMens48"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedOpexMens60" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexMens60"></td>
					</tr>
					<tr>
						<td>COGS</td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCogsInstall" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsInstall"></td>
						<!-- Valores Pontuais -->
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCogsPont1" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsPont1"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCogsPont13" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsPont13"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCogsPont25" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsPont25"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCogsPont37" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsPont37"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCogsPont49" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCogsMens12" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsMens12"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCogsMens24" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsMens24"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCogsMens36" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsMens36"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCogsMens48" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsMens48"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAllocatedCogsMens60" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsMens60"></td>
					</tr>
					<!-- ACCOMPLISHED -->
					<tr>
						<td rowspan="3" style="transform:rotate(-90deg);">REALIZADO</td>
						<td>CAPEX</td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCapexInstall" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexInstall"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCapexPont1" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexPont1"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCapexPont13" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexPont13"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCapexPont25" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexPont25"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCapexPont37" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexPont37"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCapexPont49" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCapexMens12" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexMens12"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCapexMens24" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexMens24"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCapexMens36" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexMens36"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCapexMens48" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexMens48"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCapexMens60" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexMens60"></td>
					</tr>
					<tr>
						<td>OPEX</td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedOpexInstall" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexInstall"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedOpexPont1" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexPont1"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedOpexPont13" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexPont13"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedOpexPont25" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexPont25"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedOpexPont37" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexPont37"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedOpexPont49" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedOpexMens12" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexMens12"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedOpexMens24" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexMens24"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedOpexMens36" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexMens36"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedOpexMens48" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexMens48"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedOpexMens60" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexMens60"></td>
					</tr>
					<tr>
						<td>COGS</td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCogsInstall" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsInstall"></td>
						<!-- Valores Pontuais -->
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCogsPont1" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsPont1"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCogsPont13" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsPont13"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCogsPont25" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsPont25"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCogsPont37" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsPont37"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCogsPont49" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCogsMens12" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsMens12"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCogsMens24" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsMens24"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCogsMens36" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsMens36"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCogsMens48" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsMens48"></td>
						<td><input type="number" ng-model="model.newMatrix.brlAccomplishedCogsMens60" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsMens60"></td>
					</tr>
					
					
					<!-- USD -->
					<!-- ALLOCATED -->
					<tr>
						<td rowspan="6" style="transform:rotate(-90deg);">USD</td>
						<td rowspan="3" style="transform:rotate(-90deg);">ALOCADO</td>
						<td>CAPEX</td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCapexInstall" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexInstall"></td>
						<!-- Valores Pontuais -->
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCapexPont1" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexPont1"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCapexPont13" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexPont13"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCapexPont25" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexPont25"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCapexPont37" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexPont37"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCapexPont49" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCapexMens12" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexMens12"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCapexMens24" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexMens24"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCapexMens36" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexMens36"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCapexMens48" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexMens48"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCapexMens60" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexMens60"></td>
					</tr>
					<tr>
						<td>OPEX</td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedOpexInstall" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexInstall"></td>
						<!-- Valores Pontuais -->
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedOpexPont1" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexPont1"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedOpexPont13" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexPont13"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedOpexPont25" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexPont25"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedOpexPont37" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexPont37"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedOpexPont49" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedOpexMens12" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexMens12"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedOpexMens24" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexMens24"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedOpexMens36" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexMens36"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedOpexMens48" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexMens48"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedOpexMens60" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexMens60"></td>
					</tr>
					<tr>
						<td>COGS</td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCogsInstall" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsInstall"></td>
						<!-- Valores Pontuais -->
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCogsPont1" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsPont1"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCogsPont13" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsPont13"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCogsPont25" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsPont25"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCogsPont37" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsPont37"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCogsPont49" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCogsMens12" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsMens12"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCogsMens24" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsMens24"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCogsMens36" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsMens36"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCogsMens48" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsMens48"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAllocatedCogsMens60" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsMens60"></td>
					</tr>
					<!-- ACCOMPLISHED -->
					<tr>
						<td rowspan="3" style="transform:rotate(-90deg);">REALIZADO</td>
						<td>CAPEX</td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCapexInstall" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexInstall"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCapexPont1" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexPont1"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCapexPont13" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexPont13"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCapexPont25" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexPont25"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCapexPont37" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexPont37"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCapexPont49" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCapexMens12" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexMens12"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCapexMens24" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexMens24"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCapexMens36" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexMens36"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCapexMens48" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexMens48"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCapexMens60" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexMens60"></td>
					</tr>
					<tr>
						<td>OPEX</td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedOpexInstall" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexInstall"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedOpexPont1" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexPont1"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedOpexPont13" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexPont13"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedOpexPont25" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexPont25"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedOpexPont37" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexPont37"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedOpexPont49" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedOpexMens12" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexMens12"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedOpexMens24" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexMens24"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedOpexMens36" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexMens36"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedOpexMens48" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexMens48"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedOpexMens60" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexMens60"></td>
					</tr>
					<tr>
						<td>COGS</td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCogsInstall" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsInstall"></td>
						<!-- Valores Pontuais -->
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCogsPont1" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsPont1"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCogsPont13" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsPont13"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCogsPont25" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsPont25"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCogsPont37" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsPont37"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCogsPont49" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCogsMens12" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsMens12"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCogsMens24" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsMens24"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCogsMens36" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsMens36"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCogsMens48" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsMens48"></td>
						<td><input type="number" ng-model="model.newMatrix.usdAccomplishedCogsMens60" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsMens60"></td>
					</tr>
				</table>
				<br />
				<button class="btn btn-success" ng-click="model.createMatrix()">Cadastrar</button>
				<button class="btn btn-info" ng-click="model.action='view';">Cancelar</button>
			</form>
			
			<!-- Update -->
			<form class="navbar-form navbar-left" role="search" ng-show="model.action == 'update'">
				<input type="hidden" ng-model="model.updatedMatrix.id" name="id">
				
				<label>Nome</label><br />
				<input type="text" ng-model="model.updatedMatrix.name" style="width: 90px;" class="form-control" placeholder="" name="name"><br /><br />
				
				<label>Pacote</label><br />
				<select ng-model="model.updatedMatrix.package" class="form-control">
					<option ng-repeat="package in packages" ng-if="model.isSelectedPackage(package.id)" selected="selected" value="{{package.id}}">{{package.name}}</option>
					<option ng-repeat="package in packages" ng-if="!model.isSelectedPackage(package.id)" value="{{package.id}}">{{package.name}}</option>
				</select>
				<br /><br />
				
				<table border="1">
					<tr>
						<td colspan="3" rowspan="2"></td>
						<td rowspan="2">Instala��o</td>
						<td colspan="5">Valores Pontuais</td>
						<td colspan="5">Valores Mensais conforme Prazo Contratual</td>
					</tr>
					<tr>
						<td>M�s 1</td>
						<td>M�s 13</td>
						<td>M�s 25</td>
						<td>M�s 37</td>
						<td>M�s 49</td>
						<td>12 Meses</td>
						<td>24 Meses</td>
						<td>36 Meses</td>
						<td>48 Meses</td>
						<td>60 Meses</td>
					</tr>
					
					<!-- BRL -->
					<!-- ALLOCATED -->
					<tr>
						<td rowspan="6" style="transform:rotate(-90deg);">BRL</td>
						<td rowspan="3" style="transform:rotate(-90deg);">ALOCADO</td>
						<td>CAPEX</td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCapexInstall" style="width: 90px;" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexInstall"></td>
						<!-- Valores Pontuais -->
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCapexPont1" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexPont1"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCapexPont13" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexPont13"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCapexPont25" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexPont25"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCapexPont37" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexPont37"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCapexPont49" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCapexMens12" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexMens12"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCapexMens24" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexMens24"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCapexMens36" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexMens36"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCapexMens48" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexMens48"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCapexMens60" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCapexMens60"></td>
					</tr>
					<tr>
						<td>OPEX</td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedOpexInstall" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexInstall"></td>
						<!-- Valores Pontuais -->
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedOpexPont1" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexPont1"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedOpexPont13" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexPont13"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedOpexPont25" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexPont25"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedOpexPont37" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexPont37"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedOpexPont49" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedOpexMens12" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexMens12"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedOpexMens24" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexMens24"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedOpexMens36" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexMens36"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedOpexMens48" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexMens48"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedOpexMens60" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedOpexMens60"></td>
					</tr>
					<tr>
						<td>COGS</td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCogsInstall" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsInstall"></td>
						<!-- Valores Pontuais -->
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCogsPont1" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsPont1"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCogsPont13" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsPont13"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCogsPont25" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsPont25"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCogsPont37" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsPont37"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCogsPont49" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCogsMens12" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsMens12"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCogsMens24" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsMens24"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCogsMens36" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsMens36"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCogsMens48" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsMens48"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAllocatedCogsMens60" style="width: 90px;" class="form-control" placeholder="" name="brlAllocatedCogsMens60"></td>
					</tr>
					<!-- ACCOMPLISHED -->
					<tr>
						<td rowspan="3" style="transform:rotate(-90deg);">REALIZADO</td>
						<td>CAPEX</td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCapexInstall" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexInstall"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCapexPont1" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexPont1"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCapexPont13" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexPont13"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCapexPont25" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexPont25"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCapexPont37" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexPont37"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCapexPont49" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCapexMens12" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexMens12"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCapexMens24" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexMens24"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCapexMens36" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexMens36"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCapexMens48" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexMens48"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCapexMens60" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCapexMens60"></td>
					</tr>
					<tr>
						<td>OPEX</td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedOpexInstall" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexInstall"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedOpexPont1" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexPont1"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedOpexPont13" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexPont13"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedOpexPont25" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexPont25"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedOpexPont37" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexPont37"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedOpexPont49" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedOpexMens12" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexMens12"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedOpexMens24" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexMens24"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedOpexMens36" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexMens36"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedOpexMens48" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexMens48"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedOpexMens60" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedOpexMens60"></td>
					</tr>
					<tr>
						<td>COGS</td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCogsInstall" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsInstall"></td>
						<!-- Valores Pontuais -->
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCogsPont1" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsPont1"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCogsPont13" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsPont13"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCogsPont25" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsPont25"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCogsPont37" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsPont37"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCogsPont49" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCogsMens12" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsMens12"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCogsMens24" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsMens24"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCogsMens36" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsMens36"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCogsMens48" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsMens48"></td>
						<td><input type="number" ng-model="model.updatedMatrix.brlAccomplishedCogsMens60" style="width: 90px;" class="form-control" placeholder="" name="brlAccomplishedCogsMens60"></td>
					</tr>
					
					
					<!-- USD -->
					<!-- ALLOCATED -->
					<tr>
						<td rowspan="6" style="transform:rotate(-90deg);">USD</td>
						<td rowspan="3" style="transform:rotate(-90deg);">ALOCADO</td>
						<td>CAPEX</td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCapexInstall" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexInstall"></td>
						<!-- Valores Pontuais -->
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCapexPont1" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexPont1"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCapexPont13" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexPont13"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCapexPont25" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexPont25"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCapexPont37" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexPont37"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCapexPont49" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCapexMens12" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexMens12"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCapexMens24" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexMens24"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCapexMens36" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexMens36"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCapexMens48" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexMens48"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCapexMens60" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCapexMens60"></td>
					</tr>
					<tr>
						<td>OPEX</td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedOpexInstall" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexInstall"></td>
						<!-- Valores Pontuais -->
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedOpexPont1" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexPont1"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedOpexPont13" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexPont13"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedOpexPont25" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexPont25"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedOpexPont37" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexPont37"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedOpexPont49" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedOpexMens12" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexMens12"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedOpexMens24" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexMens24"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedOpexMens36" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexMens36"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedOpexMens48" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexMens48"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedOpexMens60" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedOpexMens60"></td>
					</tr>
					<tr>
						<td>COGS</td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCogsInstall" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsInstall"></td>
						<!-- Valores Pontuais -->
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCogsPont1" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsPont1"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCogsPont13" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsPont13"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCogsPont25" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsPont25"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCogsPont37" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsPont37"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCogsPont49" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCogsMens12" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsMens12"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCogsMens24" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsMens24"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCogsMens36" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsMens36"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCogsMens48" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsMens48"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAllocatedCogsMens60" style="width: 90px;" class="form-control" placeholder="" name="usdAllocatedCogsMens60"></td>
					</tr>
					<!-- ACCOMPLISHED -->
					<tr>
						<td rowspan="3" style="transform:rotate(-90deg);">REALIZADO</td>
						<td>CAPEX</td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCapexInstall" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexInstall"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCapexPont1" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexPont1"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCapexPont13" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexPont13"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCapexPont25" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexPont25"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCapexPont37" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexPont37"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCapexPont49" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCapexMens12" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexMens12"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCapexMens24" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexMens24"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCapexMens36" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexMens36"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCapexMens48" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexMens48"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCapexMens60" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCapexMens60"></td>
					</tr>
					<tr>
						<td>OPEX</td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedOpexInstall" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexInstall"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedOpexPont1" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexPont1"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedOpexPont13" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexPont13"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedOpexPont25" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexPont25"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedOpexPont37" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexPont37"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedOpexPont49" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedOpexMens12" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexMens12"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedOpexMens24" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexMens24"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedOpexMens36" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexMens36"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedOpexMens48" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexMens48"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedOpexMens60" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedOpexMens60"></td>
					</tr>
					<tr>
						<td>COGS</td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCogsInstall" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsInstall"></td>
						<!-- Valores Pontuais -->
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCogsPont1" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsPont1"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCogsPont13" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsPont13"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCogsPont25" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsPont25"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCogsPont37" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsPont37"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCogsPont49" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsPont49"></td>
						<!-- Valores Mensais confirme Prazo Contratual -->
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCogsMens12" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsMens12"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCogsMens24" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsMens24"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCogsMens36" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsMens36"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCogsMens48" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsMens48"></td>
						<td><input type="number" ng-model="model.updatedMatrix.usdAccomplishedCogsMens60" style="width: 90px;" class="form-control" placeholder="" name="usdAccomplishedCogsMens60"></td>
					</tr>
				</table>
				<br />
				<button class="btn btn-primary" ng-click="model.confirmUpdateMatrix()">Atualizar</button>
				<button class="btn btn-info" ng-click="model.action='view';">Cancelar</button>
			</form>
			
			<!-- View -->
			<div ng-show="model.action == 'view'">
				<button class="btn btn-success" ng-click="model.openCreateMatrix()">Novo</button>
				<br /><br />
				<div class="panel panel-default">
					<div class="panel-heading">Lista de Matriz de Custo</div>
					
					<table class="table">
						<tr><th>ID<th>Matriz de Custo<th>Editar<th>Excluir
						<tr ng-repeat="matrix in matrixes">
							<td>{{matrix.id}}
							<td>{{matrix.name}}
							<td><button class="btn btn-info" ng-click="model.updateMatrix($index)">Editar</button>
							<td><button class="btn btn-danger" ng-click="model.deleteMatrix($index);">Excluir</button>
						</tr>
					</table>
					
					<div class="panel-footer"><button class="btn btn-warning" ng-click="model.deleteMatrixes();">Excluir Tudo</button></div>
				</div>
			</div>
			
		</div>
	
	</body>
</html>