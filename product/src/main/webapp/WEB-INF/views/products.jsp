<!DOCTYPE html>
<html ng-app="concilApp">
	<head>
		<script type="text/javascript" src="resources/javascript/angularjs/angular.js"></script>
		<script type="text/javascript" src="resources/javascript/angularjs/angular-route.js"></script>
		<script type="text/javascript" src="resources/javascript/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="resources/javascript/concil-app.js"></script>
		<script type="text/javascript" src="resources/javascript/app/ajax-service.js"></script>
		<script type="text/javascript" src="resources/javascript/app/products/softwares-controller.js"></script>
		<script type="text/javascript" src="./resources/bootstrap/dist/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="./resources/bootstrap/dist/css/bootstrap.min.css">
	</head>
	<body ng-controller="SoftwaresController as controller" ng-init="service.init()">
	
		<!-- Navigation Bar -->
		<nav class="navbar navbar-inverse navbar-fixed-top">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">Concil - Product</a>
		      <ul class="nav navbar-nav navbar-right">
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="index">Index</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="products">Produtos</a></li>
		            <li><a href="taxes">Taxas</a></li>
		            <li><a href="addons">Addons</a></li>
		            <li><a href="groups">Grupos</a></li>
		            <li><a href="packages">Pacotes</a></li>
		            <li><a href="matrix">Matriz de Custo</a></li>
		          </ul>
		        </li>
		      </ul>
		    </div>
		  </div>
		</nav>
		<!-- End Navigation Bar -->
		
		<div class="container">
		
			<br /><br />
		
			<h1>Produtos</h1>
			
			<div ng-show="model.isSuccess" class="alert alert-success" role="alert"><center>Opera��o realizada com sucesso!</center></div>
			<div ng-show="model.isFailure" class="alert alert-danger" role="alert"><center>Falha na opera��o!</center></div>
			
			<!-- Create -->
			<form class="navbar-form navbar-left" role="search" ng-show="model.action == 'create'">
				<div class="form-group">
					<label>Nome</label><br />
					<input type="text" ng-model="model.newSoftware.product.name" class="form-control" name="name">
					<br /><br />
					<label>Taxas</label><br />
					<select ng-model="model.newSoftware.taxes" class="form-control" multiple size="6">
						<option ng-repeat="tax in taxes" value="{{tax.id}}">{{tax.name}}</option>
					</select>
					
					<br /><br />
					<label>Addons</label><br />
					<select ng-model="model.newSoftware.addons" class="form-control" multiple size="6">
						<option ng-repeat="addon in addons" value="{{addon.id}}">{{addon.product.name}}</option>
					</select>
					
					<br /><br />
					<button class="btn btn-success" ng-click="model.createSoftware()">Cadastrar</button>
					<button class="btn btn-info" ng-click="model.action='view';">Cancelar</button>
				</div>
			</form>
			
			<!-- Update -->
			<form class="navbar-form navbar-left" role="search" ng-show="model.action == 'update'">
				<div class="form-group">
					<input type="hidden" ng-model="model.updatedSoftware.id" name="id">
					<input type="hidden" ng-model="model.updatedSoftware.product.created" name="created">
					<label>Nome</label><br />
					<input type="text" ng-model="model.updatedSoftware.product.name" class="form-control" name="name">
					<br /><br />
					<label>Taxas</label><br />
					<select ng-model="model.updatedSoftware.taxes" class="form-control" multiple size="6">
						<option ng-repeat="tax in taxes" ng-if="model.isSelectedTax(tax.id)" selected="selected" value="{{tax.id}}">{{tax.name}}</option>
						<option ng-repeat="tax in taxes" ng-if="!model.isSelectedTax(tax.id)" value="{{tax.id}}">{{tax.name}}</option>
					</select>
					<br /><br />
					<label>Addons</label><br />
					<select ng-model="model.updatedSoftware.addons" class="form-control" multiple size="6">
						<option ng-repeat="addon in addons" ng-if="model.isSelectedAddon(addon.id)" selected="selected" value="{{addon.id}}">{{addon.product.name}}</option>
						<option ng-repeat="addon in addons" ng-if="!model.isSelectedAddon(addon.id)" value="{{addon.id}}">{{addon.product.name}}</option>
					</select>
					<br /><br />
					<button class="btn btn-primary" ng-click="model.confirmUpdateSoftware()">Atualizar</button>
					<button class="btn btn-info" ng-click="model.action='view';">Cancelar</button>
				</div>
			</form>
			
			<!-- View -->
			<div ng-show="model.action == 'view'">
				<button class="btn btn-success" ng-click="model.openCreateSoftware()">Novo</button>
				<br /><br />
				<div class="panel panel-default">
					<div class="panel-heading">Lista de Produtos</div>
					
					<table class="table">
						<tr><th>ID<th>Produto<th>Editar<th>Excluir
						<tr ng-repeat="software in softwares">
							<td>{{software.id}}
							<td>{{software.product.name}}
							<td><button class="btn btn-info" ng-click="model.updateSoftware($index)">Editar</button>
							<td><button class="btn btn-danger" ng-click="model.deleteSoftware($index);">Excluir</button>
						</tr>
					</table>
					
					<div class="panel-footer"><button class="btn btn-warning" ng-click="model.deleteSoftwares();">Excluir Tudo</button></div>
				</div>
			</div>
			
		</div>
	
	</body>
</html>