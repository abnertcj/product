<!DOCTYPE html>
<html ng-app="concilApp">
	<head>
		<script type="text/javascript" src="resources/javascript/angularjs/angular.js"></script>
		<script type="text/javascript" src="resources/javascript/angularjs/angular-route.js"></script>
		<script type="text/javascript" src="resources/javascript/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="resources/javascript/concil-app.js"></script>
		<script type="text/javascript" src="resources/javascript/app/ajax-service.js"></script>
		<script type="text/javascript" src="resources/javascript/app/packages/packages-controller.js"></script>
		<script type="text/javascript" src="./resources/bootstrap/dist/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="./resources/bootstrap/dist/css/bootstrap.min.css">
	</head>
	<body ng-controller="PackagesController as controller" ng-init="service.init()">
	
		<!-- Navigation Bar -->
		<nav class="navbar navbar-inverse navbar-fixed-top">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">Concil - Product</a>
		      <ul class="nav navbar-nav navbar-right">
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="index">Index</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="products">Produtos</a></li>
		            <li><a href="taxes">Taxas</a></li>
		            <li><a href="addons">Addons</a></li>
		            <li><a href="groups">Grupos</a></li>
		            <li><a href="packages">Pacotes</a></li>
		            <li><a href="matrix">Matriz de Custo</a></li>
		          </ul>
		        </li>
		      </ul>
		    </div>
		  </div>
		</nav>
		<!-- End Navigation Bar -->
		
		<div class="container">
		
			<br /><br />
		
			<h1>Pacotes</h1>
			
			<div ng-show="model.isSuccess" class="alert alert-success" role="alert"><center>Opera��o realizada com sucesso!</center></div>
			<div ng-show="model.isFailure" class="alert alert-danger" role="alert"><center>Falha na opera��o!</center></div>
			
			<!-- Create -->
			<form class="navbar-form navbar-left" role="search" ng-show="model.action == 'create'">
				<div class="form-group">
					<label>Nome</label><br />
					<input type="text" ng-model="model.newPackage.name" class="form-control" name="name"><br /><br />
					
					<label>Produto</label><br />
					<select ng-model="model.newPackage.product" class="form-control">
						<option ng-repeat="product in products" value="{{product.id}}">{{product.name}}</option>
					</select><br /><br />
					
					<label>Volume</label><br />
					<input type="number" ng-model="model.newPackage.volume" class="form-control" name="name"><br /><br />
					
					<label>Unidade</label><br />
					<input type="number" ng-model="model.newPackage.unit" class="form-control" name="name"><br /><br />
					
					<label>Pre�o</label><br />
					<input type="number" ng-model="model.newPackage.price" class="form-control" name="name"><br /><br />
					
					<label>Recorrente (Recurrent)</label><br />
					<input type="text" ng-model="model.newPackage.recurrent" class="form-control" name="name"><br /><br />
					
					<label>Configura��o (Setup)</label><br />
					<input type="text" ng-model="model.newPackage.setup" class="form-control" name="name"><br /><br />
					
					<label>Customiza��o (Custom)</label><br />
					<input type="text" ng-model="model.newPackage.custom" class="form-control" name="name"><br /><br />
					
					<label>Pre�o de Configura��o (Setup_Price)</label><br />
					<input type="number" ng-model="model.newPackage.setupPrice" class="form-control" name="name"><br /><br />
					
					<label>Pre�o recorrente (Recurrence_Price)</label><br />
					<input type="number" ng-model="model.newPackage.recurrencePrice" class="form-control" name="name"><br /><br />
					
					<button class="btn btn-success" ng-click="model.createPackage()">Cadastrar</button>
					<button class="btn btn-info" ng-click="model.action='view';">Cancelar</button>
				</div>
			</form>
			
			<!-- Update -->
			<form class="navbar-form navbar-left" role="search" ng-show="model.action == 'update'">
				<div class="form-group">
					<input type="hidden" ng-model="model.updatedPackage.id" name="id">
					<input type="hidden" ng-model="model.updatedPackage.created" name="created">
					
					<label>Nome</label><br />
					<input type="text" ng-model="model.updatedPackage.name" class="form-control" placeholder="Nome" name="name"><br /><br />
					
					<label>Produto</label><br />
					<select ng-model="model.updatedPackage.product" class="form-control">
						<option ng-repeat="product in products" ng-if="model.isSelectedProduct(product.id)" selected="selected" value="{{product.id}}">{{product.name}}</option>
						<option ng-repeat="product in products" ng-if="!model.isSelectedProduct(product.id)" value="{{product.id}}">{{product.name}}</option>
					</select><br /><br />
					
					<label>Volume</label><br />
					<input type="number" ng-model="model.updatedPackage.volume" class="form-control" placeholder="Nome" name="name"><br /><br />
					
					<label>Unidade</label><br />
					<input type="number" ng-model="model.updatedPackage.unit" class="form-control" placeholder="Nome" name="name"><br /><br />
					
					<label>Pre�o</label><br />
					<input type="number" ng-model="model.updatedPackage.price" class="form-control" placeholder="Nome" name="name"><br /><br />
					
					<label>Recorrente (Recurrent)</label><br />
					<input type="text" ng-model="model.updatedPackage.recurrent" class="form-control" placeholder="Nome" name="name"><br /><br />
					
					<label>Configura��o (Setup)</label><br />
					<input type="text" ng-model="model.updatedPackage.setup" class="form-control" placeholder="Nome" name="name"><br /><br />
					
					<label>Customiza��o (Custom)</label><br />
					<input type="text" ng-model="model.updatedPackage.custom" class="form-control" placeholder="Nome" name="name"><br /><br />
					
					<label>Pre�o de Configura��o (Setup_Price)</label><br />
					<input type="number" ng-model="model.updatedPackage.setupPrice" class="form-control" placeholder="Nome" name="name"><br /><br />
					
					<label>Pre�o recorrente (Recurrence_Price)</label><br />
					<input type="number" ng-model="model.updatedPackage.recurrencePrice" class="form-control" placeholder="Nome" name="name"><br /><br />
					
					<button class="btn btn-primary" ng-click="model.confirmUpdatePackage()">Atualizar</button>
					<button class="btn btn-info" ng-click="model.action='view';">Cancelar</button>
				</div>
			</form>
			
			<!-- View -->
			<div ng-show="model.action == 'view'">
				<button class="btn btn-success" ng-click="model.openCreatePackage()">Novo</button>
				<br /><br />
				<div class="panel panel-default">
					<div class="panel-heading">Lista de Pacotes</div>
					
					<table class="table">
						<tr><th>ID<th>Pacote<th>Editar<th>Excluir
						<tr ng-repeat="package in packages">
							<td>{{package.id}}
							<td>{{package.name}}
							<td><button class="btn btn-info" ng-click="model.updatePackage($index)">Editar</button>
							<td><button class="btn btn-danger" ng-click="model.deletePackage($index);">Excluir</button>
						</tr>
					</table>
					
					<div class="panel-footer"><button class="btn btn-warning" ng-click="model.deletePackages();">Excluir Tudo</button></div>
				</div>
			</div>
			
		</div>
	
	</body>
</html>