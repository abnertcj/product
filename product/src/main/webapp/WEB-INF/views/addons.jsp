<!DOCTYPE html>
<html ng-app="concilApp">
	<head>
		<script type="text/javascript" src="resources/javascript/angularjs/angular.js"></script>
		<script type="text/javascript" src="resources/javascript/angularjs/angular-route.js"></script>
		<script type="text/javascript" src="resources/javascript/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="resources/javascript/concil-app.js"></script>
		<script type="text/javascript" src="resources/javascript/app/ajax-service.js"></script>
		<script type="text/javascript" src="resources/javascript/app/addons/addons-controller.js"></script>
		<script type="text/javascript" src="./resources/bootstrap/dist/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="./resources/bootstrap/dist/css/bootstrap.min.css">
	</head>
	<body ng-controller="AddonsController as controller" ng-init="service.init()">
	
		<!-- Navigation Bar -->
		<nav class="navbar navbar-inverse navbar-fixed-top">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">Concil - Product</a>
		      <ul class="nav navbar-nav navbar-right">
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="index">Index</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="products">Produtos</a></li>
		            <li><a href="taxes">Taxas</a></li>
		            <li><a href="addons">Addons</a></li>
		            <li><a href="groups">Grupos</a></li>
		            <li><a href="packages">Pacotes</a></li>
		            <li><a href="matrix">Matriz de Custo</a></li>
		          </ul>
		        </li>
		      </ul>
		    </div>
		  </div>
		</nav>
		<!-- End Navigation Bar -->
		
		<div class="container">
		
			<br /><br />
		
			<h1>Addons</h1>
			
			<div ng-show="model.isSuccess" class="alert alert-success" role="alert"><center>Opera��o realizada com sucesso!</center></div>
			<div ng-show="model.isFailure" class="alert alert-danger" role="alert"><center>Falha na opera��o!</center></div>
			
			<!-- Create -->
			<form class="navbar-form navbar-left" role="search" ng-show="model.action == 'create'">
				<div class="form-group">
					<label>Nome</label><br />
					<input type="text" ng-model="model.newAddon.product.name" class="form-control" name="name"><br /><br />
					<label>Descri��o</label><br />
					<textarea type="text" ng-model="model.newAddon.description" class="form-control" name="description"></textarea><br /><br />
					<button class="btn btn-success" ng-click="model.createAddon()">Cadastrar</button>
					<button class="btn btn-info" ng-click="model.action='view';">Cancelar</button>
				</div>
			</form>
			
			<!-- Update -->
			<form class="navbar-form navbar-left" role="search" ng-show="model.action == 'update'">
				<div class="form-group">
					<input type="hidden" ng-model="model.updatedAddon.id" name="id">
					<input type="hidden" ng-model="model.updatedAddon.product.created" name="created">
					<label>Nome</label><br />
					<input type="text" ng-model="model.updatedAddon.product.name" class="form-control" placeholder="Nome" name="name"><br /><br />
					<label>Descri��o</label><br />
					<textarea type="text" ng-model="model.updatedAddon.description" class="form-control" name="description"></textarea><br /><br />
					<button class="btn btn-primary" ng-click="model.confirmUpdateAddon()">Atualizar</button>
					<button class="btn btn-info" ng-click="model.action='view';">Cancelar</button>
				</div>
			</form>
			
			<!-- View -->
			<div ng-show="model.action == 'view'">
				<button class="btn btn-success" ng-click="model.openCreateAddon()">Novo</button>
				<br /><br />
				<div class="panel panel-default">
					<div class="panel-heading">Lista de Addons</div>
					
					<table class="table">
						<tr><th>ID<th>Addon<th>Editar<th>Excluir
						<tr ng-repeat="addon in addons">
							<td>{{addon.id}}
							<td>{{addon.product.name}}
							<td><button class="btn btn-info" ng-click="model.updateAddon($index)">Editar</button>
							<td><button class="btn btn-danger" ng-click="model.deleteAddon($index);">Excluir</button>
						</tr>
					</table>
					
					<div class="panel-footer"><button class="btn btn-warning" ng-click="model.deleteAddons();">Excluir Tudo</button></div>
				</div>
			</div>
			
		</div>
	
	</body>
</html>