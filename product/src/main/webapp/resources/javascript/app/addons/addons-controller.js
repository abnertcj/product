'use strict';
var model = model || {};
var service = service || {};
concilApp.controller('AddonsController', function($scope, AjaxService) {
	
	service = AjaxService;
	
	// Get All
	$scope.addons = [];
	service.init = function() {
		$scope.addons = service.httpGet('api/addon', model.getAllCallback, model.generalFailureCallback);
	}
	model.isSuccess = false;
	model.isFailure = false;
	
	// Post
	model.action = 'view';
	model.newAddon = {
		product: {
			name: null	
		},
		description: ''
	};
	model.openCreateAddon = function() {
		model.action='create';
		model.isSuccess = false;
		model.isFailure = false;
	};
	model.createAddon = function() {
		service.httpPost('api/addon', model.newAddon, model.postCallback, model.generalFailureCallback);
	}
	
	// Put
	model.updatedAddon = {
		id: null,
		product: {
			name: null,
			created: null
		},
		description: ''
	};
	model.updateAddon = function(index) {
		var addon = $scope.addons[index];
		model.updatedAddon.id = addon.id;
		model.updatedAddon.product.name = addon.product.name;
		model.updatedAddon.product.created = addon.product.created;
		model.updatedAddon.description = addon.description;
		model.action = 'update';
		model.isSuccess = false;
		model.isFailure = false;
	};
	model.confirmUpdateAddon = function() {
		service.httpPut('api/addon/' + model.updatedAddon.id, model.updatedAddon, model.putCallback, model.generalFailureCallback);
		for (var i = 0; i <= $scope.addons.length; i ++) {
			if ($scope.addons[i].id == model.updatedAddon.id) {
				$scope.addons[i].id = model.updatedAddon.id;
				$scope.addons[i].product.name = model.updatedAddon.product.name;
				$scope.addons[i].product.created = model.updatedAddon.product.created;
				$scope.addons[i].description = model.updatedAddon.description;
				break;
			}
		}
		model.action = 'view';
	};
	
	// Delete
	model.deleteAddon = function(index) {
		var id = $scope.addons[index].id;
		service.httpDelete('api/addon/' + id, model.deleteCallback, model.generalFailureCallback, index);
	};
	
	model.deleteAddons = function() {
		service.httpDeleteAll('api/addon', model.deleteAllCallback, model.generalFailureCallback);
	};
	
	/*
	 * Callbacks
	 */
	
	model.getCallback = function(data) {
		
	};
	
	model.getAllCallback = function(data) {
		$scope.addons = data;
	};
	
	model.postCallback = function(data) {
		$scope.addons = $scope.addons || [];
		$scope.addons.push(data);
		model.newAddon.product.name = null;
		model.newAddon.description = null;
		model.action = 'view';
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.putCallback = function(data) {
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.deleteCallback = function(data, index) {
		$scope.addons.splice(index, 1);
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.deleteAllCallback = function() {
		$scope.addons = [];
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.generalFailureCallback = function(data) {
		console.log('Failure');
		model.isSuccess = false;
		model.isFailure = true;
	};
	
	$scope.model = model;
	$scope.service = service;
});