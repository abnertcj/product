'use strict';
var model = model || {};
var service = service || {};
concilApp.controller('TaxesController', function($scope, AjaxService) {
	
	service = AjaxService;
	
	// Get All
	$scope.taxes = [];
	service.init = function() {
		$scope.taxes = service.httpGet('api/tax', model.getAllCallback, model.generalFailureCallback);
	}
	model.isSuccess = false;
	model.isFailure = false;
	
	// Post
	model.action = 'view';
	model.newTax = {
		name: null,
		tax: null
	};
	model.openCreateTax = function() {
		model.action='create';
		model.isSuccess = false;
		model.isFailure = false;
	};
	model.createTax = function() {
		service.httpPost('api/tax', model.newTax, model.postCallback, model.generalFailureCallback);
	}
	
	// Put
	model.updatedTax = {
		id: null,
		name: null,
		tax: null,
		created: null
	};
	model.updateTax = function(index) {
		var tax = $scope.taxes[index];
		model.updatedTax.id = tax.id;
		model.updatedTax.name = tax.name;
		model.updatedTax.tax = tax.tax;
		model.updatedTax.created = tax.created;
		model.action = 'update';
		model.isSuccess = false;
		model.isFailure = false;
	};
	model.confirmUpdateTax = function() {
		service.httpPut('api/tax/' + model.updatedTax.id, model.updatedTax, model.putCallback, model.generalFailureCallback);
		for (var i = 0; i <= $scope.taxes.length; i ++) {
			if ($scope.taxes[i].id == model.updatedTax.id) {
				$scope.taxes[i].id = model.updatedTax.id;
				$scope.taxes[i].name = model.updatedTax.name;
				$scope.taxes[i].tax = model.updatedTax.tax;
				$scope.taxes[i].created = model.updatedTax.created;
				break;
			}
		}
		model.action = 'view';
	};
	
	// Delete
	model.deleteTax = function(index) {
		var id = $scope.taxes[index].id;
		service.httpDelete('api/tax/' + id, model.deleteCallback, model.generalFailureCallback, index);
	};
	
	model.deleteTaxes = function() {
		service.httpDeleteAll('api/tax', model.deleteAllCallback, model.generalFailureCallback);
	};
	
	/*
	 * Callbacks
	 */
	
	model.getCallback = function(data) {
		
	};
	
	model.getAllCallback = function(data) {
		$scope.taxes = data;
	};
	
	model.postCallback = function(data) {
		$scope.taxes = $scope.taxes || [];
		$scope.taxes.push(data);
		model.newTax.name = null;
		model.action = 'view';
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.putCallback = function(data) {
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.deleteCallback = function(data, index) {
		$scope.taxes.splice(index, 1);
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.deleteAllCallback = function() {
		$scope.taxes = [];
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.generalFailureCallback = function(data) {
		console.log('Failure');
		model.isSuccess = false;
		model.isFailure = true;
	};
	
	$scope.model = model;
	$scope.service = service;
});