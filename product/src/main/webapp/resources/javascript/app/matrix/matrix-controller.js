'use strict';
var model = model || {};
var service = service || {};
concilApp.controller('MatrixController', function($scope, AjaxService) {
	
	service = AjaxService;
	
	// Get All
	$scope.matrixes = [];
	service.init = function() {
		service.httpGet('api/matrix', model.getAllCallback, model.generalFailureCallback);
		service.httpGet('api/package', model.getAllPackageCallback, model.generalFailureCallback);
	}
	model.isSuccess = false;
	model.isFailure = false;
	
	// Post
	model.action = 'view';
	model.newMatrix = {
		name: null,
		
		/* BRL */
		
		// CAPEX
		// Valores Pontuais
		brlAllocatedCapexInstall: null,
		brlAllocatedCapexPont1: null,
		brlAllocatedCapexPont13: null,
		brlAllocatedCapexPont25: null,
		brlAllocatedCapexPont37: null,
		brlAllocatedCapexPont49: null,
	
		// Valores Mensais confirme Prazo Contratual
		brlAllocatedCapexMens12: null,
		brlAllocatedCapexMens24: null,
		brlAllocatedCapexMens36: null,
		brlAllocatedCapexMens48: null,
		brlAllocatedCapexMens60: null,
		
		// OPEX
		// Valores Pontuais
		brlAllocatedOpexInstall: null,
		brlAllocatedOpexPont1: null,
		brlAllocatedOpexPont13: null,
		brlAllocatedOpexPont25: null,
		brlAllocatedOpexPont37: null,
		brlAllocatedOpexPont49: null,
	
		// Valores Mensais confirme Prazo Contratual
		brlAllocatedOpexMens12: null,
		brlAllocatedOpexMens24: null,
		brlAllocatedOpexMens36: null,
		brlAllocatedOpexMens48: null,
		brlAllocatedOpexMens60: null,
		
		// COGS
		// Valores Pontuais
		brlAllocatedCogsInstall: null,
		brlAllocatedCogsPont1: null,
		brlAllocatedCogsPont13: null,
		brlAllocatedCogsPont25: null,
		brlAllocatedCogsPont37: null,
		brlAllocatedCogsPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		brlAllocatedCogsMens12: null,
		brlAllocatedCogsMens24: null,
		brlAllocatedCogsMens36: null,
		brlAllocatedCogsMens48: null,
		brlAllocatedCogsMens60: null,
		
		// ACCOMPLISHED
		
		// CAPEX
		// Valores Pontuais
		brlAccomplishedCapexInstall: null,
		brlAccomplishedCapexPont1: null,
		brlAccomplishedCapexPont13: null,
		brlAccomplishedCapexPont25: null,
		brlAccomplishedCapexPont37: null,
		brlAccomplishedCapexPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		brlAccomplishedCapexMens12: null,
		brlAccomplishedCapexMens24: null,
		brlAccomplishedCapexMens36: null,
		brlAccomplishedCapexMens48: null,
		brlAccomplishedCapexMens60: null,
		
		// OPEX
		// Valores Pontuais
		brlAccomplishedOpexInstall: null,
		brlAccomplishedOpexPont1: null,
		brlAccomplishedOpexPont13: null,
		brlAccomplishedOpexPont25: null,
		brlAccomplishedOpexPont37: null,
		brlAccomplishedOpexPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		brlAccomplishedOpexMens12: null,
		brlAccomplishedOpexMens24: null,
		brlAccomplishedOpexMens36: null,
		brlAccomplishedOpexMens48: null,
		brlAccomplishedOpexMens60: null,
		
		// COGS
		// Valores Pontuais
		brlAccomplishedCogsInstall: null,
		brlAccomplishedCogsPont1: null,
		brlAccomplishedCogsPont13: null,
		brlAccomplishedCogsPont25: null,
		brlAccomplishedCogsPont37: null,
		brlAccomplishedCogsPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		brlAccomplishedCogsMens12: null,
		brlAccomplishedCogsMens24: null,
		brlAccomplishedCogsMens36: null,
		brlAccomplishedCogsMens48: null,
		brlAccomplishedCogsMens60: null,
		
		/* USD */
		
		// ALLOCATED
		
		// CAPEX
		// Valores Pontuais
		usdAllocatedCapexInstall: null,
		usdAllocatedCapexPont1: null,
		usdAllocatedCapexPont13: null,
		usdAllocatedCapexPont25: null,
		usdAllocatedCapexPont37: null,
		usdAllocatedCapexPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		usdAllocatedCapexMens12: null,
		usdAllocatedCapexMens24: null,
		usdAllocatedCapexMens36: null,
		usdAllocatedCapexMens48: null,
		usdAllocatedCapexMens60: null,
		
		// OPEX
		// Valores Pontuais
		usdAllocatedOpexInstall: null,
		usdAllocatedOpexPont1: null,
		usdAllocatedOpexPont13: null,
		usdAllocatedOpexPont25: null,
		usdAllocatedOpexPont37: null,
		usdAllocatedOpexPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		usdAllocatedOpexMens12: null,
		usdAllocatedOpexMens24: null,
		usdAllocatedOpexMens36: null,
		usdAllocatedOpexMens48: null,
		usdAllocatedOpexMens60: null,
		
		// COGS
		// Valores Pontuais
		usdAllocatedCogsInstall: null,
		usdAllocatedCogsPont1: null,
		usdAllocatedCogsPont13: null,
		usdAllocatedCogsPont25: null,
		usdAllocatedCogsPont37: null,
		usdAllocatedCogsPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		usdAllocatedCogsMens12: null,
		usdAllocatedCogsMens24: null,
		usdAllocatedCogsMens36: null,
		usdAllocatedCogsMens48: null,
		usdAllocatedCogsMens60: null,
		
		// ACCOMPLISHED
		
		// CAPEX
		// Valores Pontuais
		usdAccomplishedCapexInstall: null,
		usdAccomplishedCapexPont1: null,
		usdAccomplishedCapexPont13: null,
		usdAccomplishedCapexPont25: null,
		usdAccomplishedCapexPont37: null,
		usdAccomplishedCapexPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		usdAccomplishedCapexMens12: null,
		usdAccomplishedCapexMens24: null,
		usdAccomplishedCapexMens36: null,
		usdAccomplishedCapexMens48: null,
		usdAccomplishedCapexMens60: null,
		
		// OPEX
		// Valores Pontuais
		usdAccomplishedOpexInstall: null,
		usdAccomplishedOpexPont1: null,
		usdAccomplishedOpexPont13: null,
		usdAccomplishedOpexPont25: null,
		usdAccomplishedOpexPont37: null,
		usdAccomplishedOpexPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		usdAccomplishedOpexMens12: null,
		usdAccomplishedOpexMens24: null,
		usdAccomplishedOpexMens36: null,
		usdAccomplishedOpexMens48: null,
		usdAccomplishedOpexMens60: null,
		
		// COGS
		// Valores Pontuais
		usdAccomplishedCogsInstall: null,
		usdAccomplishedCogsPont1: null,
		usdAccomplishedCogsPont13: null,
		usdAccomplishedCogsPont25: null,
		usdAccomplishedCogsPont37: null,
		usdAccomplishedCogsPont49: null,
	
		// Valores Mensais confirme Prazo Contratual
		usdAccomplishedCogsMens12: null,
		usdAccomplishedCogsMens24: null,
		usdAccomplishedCogsMens36: null,
		usdAccomplishedCogsMens48: null,
		usdAccomplishedCogsMens60: null
	};
	model.openCreateMatrix = function() {
		model.action='create';
		model.isSuccess = false;
		model.isFailure = false;
	};
	model.createMatrix = function() {
		var selectedPackage = null;
		for (var i = 0; i < $scope.packages.length; i ++) {// All Packages
			if ($scope.packages[i].id == model.newMatrix.package) {
				selectedPackage = $scope.packages[i];
			}
		}
		model.newMatrix.package = selectedPackage;
		
		service.httpPost('api/matrix', model.newMatrix, model.postCallback, model.generalFailureCallback);
	}
	
	// Put
	model.updatedMatrix = {
		id: null,
		name: null,
		created: null,
		brlAllocatedCapexInstall: null,
		brlAllocatedCapexPont1: null,
		brlAllocatedCapexPont13: null,
		brlAllocatedCapexPont25: null,
		brlAllocatedCapexPont37: null,
		brlAllocatedCapexPont49: null,
	
		// Valores Mensais confirme Prazo Contratual
		brlAllocatedCapexMens12: null,
		brlAllocatedCapexMens24: null,
		brlAllocatedCapexMens36: null,
		brlAllocatedCapexMens48: null,
		brlAllocatedCapexMens60: null,
		
		// OPEX
		// Valores Pontuais
		brlAllocatedOpexInstall: null,
		brlAllocatedOpexPont1: null,
		brlAllocatedOpexPont13: null,
		brlAllocatedOpexPont25: null,
		brlAllocatedOpexPont37: null,
		brlAllocatedOpexPont49: null,
	
		// Valores Mensais confirme Prazo Contratual
		brlAllocatedOpexMens12: null,
		brlAllocatedOpexMens24: null,
		brlAllocatedOpexMens36: null,
		brlAllocatedOpexMens48: null,
		brlAllocatedOpexMens60: null,
		
		// COGS
		// Valores Pontuais
		brlAllocatedCogsInstall: null,
		brlAllocatedCogsPont1: null,
		brlAllocatedCogsPont13: null,
		brlAllocatedCogsPont25: null,
		brlAllocatedCogsPont37: null,
		brlAllocatedCogsPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		brlAllocatedCogsMens12: null,
		brlAllocatedCogsMens24: null,
		brlAllocatedCogsMens36: null,
		brlAllocatedCogsMens48: null,
		brlAllocatedCogsMens60: null,
		
		// ACCOMPLISHED
		
		// CAPEX
		// Valores Pontuais
		brlAccomplishedCapexInstall: null,
		brlAccomplishedCapexPont1: null,
		brlAccomplishedCapexPont13: null,
		brlAccomplishedCapexPont25: null,
		brlAccomplishedCapexPont37: null,
		brlAccomplishedCapexPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		brlAccomplishedCapexMens12: null,
		brlAccomplishedCapexMens24: null,
		brlAccomplishedCapexMens36: null,
		brlAccomplishedCapexMens48: null,
		brlAccomplishedCapexMens60: null,
		
		// OPEX
		// Valores Pontuais
		brlAccomplishedOpexInstall: null,
		brlAccomplishedOpexPont1: null,
		brlAccomplishedOpexPont13: null,
		brlAccomplishedOpexPont25: null,
		brlAccomplishedOpexPont37: null,
		brlAccomplishedOpexPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		brlAccomplishedOpexMens12: null,
		brlAccomplishedOpexMens24: null,
		brlAccomplishedOpexMens36: null,
		brlAccomplishedOpexMens48: null,
		brlAccomplishedOpexMens60: null,
		
		// COGS
		// Valores Pontuais
		brlAccomplishedCogsInstall: null,
		brlAccomplishedCogsPont1: null,
		brlAccomplishedCogsPont13: null,
		brlAccomplishedCogsPont25: null,
		brlAccomplishedCogsPont37: null,
		brlAccomplishedCogsPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		brlAccomplishedCogsMens12: null,
		brlAccomplishedCogsMens24: null,
		brlAccomplishedCogsMens36: null,
		brlAccomplishedCogsMens48: null,
		brlAccomplishedCogsMens60: null,
		
		/* USD */
		
		// ALLOCATED
		
		// CAPEX
		// Valores Pontuais
		usdAllocatedCapexInstall: null,
		usdAllocatedCapexPont1: null,
		usdAllocatedCapexPont13: null,
		usdAllocatedCapexPont25: null,
		usdAllocatedCapexPont37: null,
		usdAllocatedCapexPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		usdAllocatedCapexMens12: null,
		usdAllocatedCapexMens24: null,
		usdAllocatedCapexMens36: null,
		usdAllocatedCapexMens48: null,
		usdAllocatedCapexMens60: null,
		
		// OPEX
		// Valores Pontuais
		usdAllocatedOpexInstall: null,
		usdAllocatedOpexPont1: null,
		usdAllocatedOpexPont13: null,
		usdAllocatedOpexPont25: null,
		usdAllocatedOpexPont37: null,
		usdAllocatedOpexPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		usdAllocatedOpexMens12: null,
		usdAllocatedOpexMens24: null,
		usdAllocatedOpexMens36: null,
		usdAllocatedOpexMens48: null,
		usdAllocatedOpexMens60: null,
		
		// COGS
		// Valores Pontuais
		usdAllocatedCogsInstall: null,
		usdAllocatedCogsPont1: null,
		usdAllocatedCogsPont13: null,
		usdAllocatedCogsPont25: null,
		usdAllocatedCogsPont37: null,
		usdAllocatedCogsPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		usdAllocatedCogsMens12: null,
		usdAllocatedCogsMens24: null,
		usdAllocatedCogsMens36: null,
		usdAllocatedCogsMens48: null,
		usdAllocatedCogsMens60: null,
		
		// ACCOMPLISHED
		
		// CAPEX
		// Valores Pontuais
		usdAccomplishedCapexInstall: null,
		usdAccomplishedCapexPont1: null,
		usdAccomplishedCapexPont13: null,
		usdAccomplishedCapexPont25: null,
		usdAccomplishedCapexPont37: null,
		usdAccomplishedCapexPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		usdAccomplishedCapexMens12: null,
		usdAccomplishedCapexMens24: null,
		usdAccomplishedCapexMens36: null,
		usdAccomplishedCapexMens48: null,
		usdAccomplishedCapexMens60: null,
		
		// OPEX
		// Valores Pontuais
		usdAccomplishedOpexInstall: null,
		usdAccomplishedOpexPont1: null,
		usdAccomplishedOpexPont13: null,
		usdAccomplishedOpexPont25: null,
		usdAccomplishedOpexPont37: null,
		usdAccomplishedOpexPont49: null,
		
		// Valores Mensais confirme Prazo Contratual
		usdAccomplishedOpexMens12: null,
		usdAccomplishedOpexMens24: null,
		usdAccomplishedOpexMens36: null,
		usdAccomplishedOpexMens48: null,
		usdAccomplishedOpexMens60: null,
		
		// COGS
		// Valores Pontuais
		usdAccomplishedCogsInstall: null,
		usdAccomplishedCogsPont1: null,
		usdAccomplishedCogsPont13: null,
		usdAccomplishedCogsPont25: null,
		usdAccomplishedCogsPont37: null,
		usdAccomplishedCogsPont49: null,
	
		// Valores Mensais confirme Prazo Contratual
		usdAccomplishedCogsMens12: null,
		usdAccomplishedCogsMens24: null,
		usdAccomplishedCogsMens36: null,
		usdAccomplishedCogsMens48: null,
		usdAccomplishedCogsMens60: null
	};
	model.updateMatrix = function(index) {
		var matrix = $scope.matrixes[index];
		model.updatedMatrix.id = matrix.id;
		model.updatedMatrix.name = matrix.name;
		model.updatedMatrix.created = matrix.created;
		
		// BRL
		
		// ALLOCATED
		
		// CAPEX
		// Valores Pontuais
		model.updatedMatrix.brlAllocatedCapexInstall = matrix.brlAllocatedCapexInstall;
		model.updatedMatrix.brlAllocatedCapexPont1 = matrix.brlAllocatedCapexPont1;
		model.updatedMatrix.brlAllocatedCapexPont13 = matrix.brlAllocatedCapexPont13;
		model.updatedMatrix.brlAllocatedCapexPont25 = matrix.brlAllocatedCapexPont25;
		model.updatedMatrix.brlAllocatedCapexPont37 = matrix.brlAllocatedCapexPont37;
		model.updatedMatrix.brlAllocatedCapexPont49 = matrix.brlAllocatedCapexPont49;
	
		// Valores Mensais confirme Prazo Contratual
		model.updatedMatrix.brlAllocatedCapexMens12 = matrix.brlAllocatedCapexMens12;
		model.updatedMatrix.brlAllocatedCapexMens24 = matrix.brlAllocatedCapexMens24;
		model.updatedMatrix.brlAllocatedCapexMens36 = matrix.brlAllocatedCapexMens36;
		model.updatedMatrix.brlAllocatedCapexMens48 = matrix.brlAllocatedCapexMens48;
		model.updatedMatrix.brlAllocatedCapexMens60 = matrix.brlAllocatedCapexMens60;
		
		// OPEX
		// Valores Pontuais
		model.updatedMatrix.brlAllocatedOpexInstall = matrix.brlAllocatedOpexInstall;
		model.updatedMatrix.brlAllocatedOpexPont1 = matrix.brlAllocatedOpexPont1;
		model.updatedMatrix.brlAllocatedOpexPont13 = matrix.brlAllocatedOpexPont13;
		model.updatedMatrix.brlAllocatedOpexPont25 = matrix.brlAllocatedOpexPont25;
		model.updatedMatrix.brlAllocatedOpexPont37 = matrix.brlAllocatedOpexPont37;
		model.updatedMatrix.brlAllocatedOpexPont49 = matrix.brlAllocatedOpexPont49;
	
		// Valores Mensais confirme Prazo Contratual
		model.updatedMatrix.brlAllocatedOpexMens12 = matrix.brlAllocatedOpexMens12;
		model.updatedMatrix.brlAllocatedOpexMens24 = matrix.brlAllocatedOpexMens24;
		model.updatedMatrix.brlAllocatedOpexMens36 = matrix.brlAllocatedOpexMens36;
		model.updatedMatrix.brlAllocatedOpexMens48 = matrix.brlAllocatedOpexMens48;
		model.updatedMatrix.brlAllocatedOpexMens60 = matrix.brlAllocatedOpexMens60;
		
		// COGS
		// Valores Pontuais
		model.updatedMatrix.brlAllocatedCogsInstall = matrix.brlAllocatedCogsInstall;
		model.updatedMatrix.brlAllocatedCogsPont1 = matrix.brlAllocatedCogsPont1;
		model.updatedMatrix.brlAllocatedCogsPont13 = matrix.brlAllocatedCogsPont13;
		model.updatedMatrix.brlAllocatedCogsPont25 = matrix.brlAllocatedCogsPont25;
		model.updatedMatrix.brlAllocatedCogsPont37 = matrix.brlAllocatedCogsPont37;
		model.updatedMatrix.brlAllocatedCogsPont49 = matrix.brlAllocatedCogsPont49;
	
		// Valores Mensais confirme Prazo Contratual
		model.updatedMatrix.brlAllocatedCogsMens12 = matrix.brlAllocatedCogsMens12;
		model.updatedMatrix.brlAllocatedCogsMens24 = matrix.brlAllocatedCogsMens24;
		model.updatedMatrix.brlAllocatedCogsMens36 = matrix.brlAllocatedCogsMens36;
		model.updatedMatrix.brlAllocatedCogsMens48 = matrix.brlAllocatedCogsMens48;
		model.updatedMatrix.brlAllocatedCogsMens60 = matrix.brlAllocatedCogsMens60;
		
		// ACCOMPLISHED
		
		// CAPEX
		// Valores Pontuais
		model.updatedMatrix.brlAccomplishedCapexInstall = matrix.brlAccomplishedCapexInstall;
		model.updatedMatrix.brlAccomplishedCapexPont1 = matrix.brlAccomplishedCapexPont1;
		model.updatedMatrix.brlAccomplishedCapexPont13 = matrix.brlAccomplishedCapexPont13;
		model.updatedMatrix.brlAccomplishedCapexPont25 = matrix.brlAccomplishedCapexPont25;
		model.updatedMatrix.brlAccomplishedCapexPont37 = matrix.brlAccomplishedCapexPont37;
		model.updatedMatrix.brlAccomplishedCapexPont49 = matrix.brlAccomplishedCapexPont49;
	
		// Valores Mensais confirme Prazo Contratual
		model.updatedMatrix.brlAccomplishedCapexMens12 = matrix.brlAccomplishedCapexMens12;
		model.updatedMatrix.brlAccomplishedCapexMens24 = matrix.brlAccomplishedCapexMens24;
		model.updatedMatrix.brlAccomplishedCapexMens36 = matrix.brlAccomplishedCapexMens36;
		model.updatedMatrix.brlAccomplishedCapexMens48 = matrix.brlAccomplishedCapexMens48;
		model.updatedMatrix.brlAccomplishedCapexMens60 = matrix.brlAccomplishedCapexMens60;
		
		// OPEX
		// Valores Pontuais
		model.updatedMatrix.brlAccomplishedOpexInstall = matrix.brlAccomplishedOpexInstall;
		model.updatedMatrix.brlAccomplishedOpexPont1 = matrix.brlAccomplishedOpexPont1;
		model.updatedMatrix.brlAccomplishedOpexPont13 = matrix.brlAccomplishedOpexPont13;
		model.updatedMatrix.brlAccomplishedOpexPont25 = matrix.brlAccomplishedOpexPont25;
		model.updatedMatrix.brlAccomplishedOpexPont37 = matrix.brlAccomplishedOpexPont37;
		model.updatedMatrix.brlAccomplishedOpexPont49 = matrix.brlAccomplishedOpexPont49;
	
		// Valores Mensais confirme Prazo Contratual
		model.updatedMatrix.brlAccomplishedOpexMens12 = matrix.brlAccomplishedOpexMens12;
		model.updatedMatrix.brlAccomplishedOpexMens24 = matrix.brlAccomplishedOpexMens24;
		model.updatedMatrix.brlAccomplishedOpexMens36 = matrix.brlAccomplishedOpexMens36;
		model.updatedMatrix.brlAccomplishedOpexMens48 = matrix.brlAccomplishedOpexMens48;
		model.updatedMatrix.brlAccomplishedOpexMens60 = matrix.brlAccomplishedOpexMens60;
		
		// COGS
		// Valores Pontuais
		model.updatedMatrix.brlAccomplishedCogsInstall = matrix.brlAccomplishedCogsInstall;
		model.updatedMatrix.brlAccomplishedCogsPont1 = matrix.brlAccomplishedCogsPont1;
		model.updatedMatrix.brlAccomplishedCogsPont13 = matrix.brlAccomplishedCogsPont13;
		model.updatedMatrix.brlAccomplishedCogsPont25 = matrix.brlAccomplishedCogsPont25;
		model.updatedMatrix.brlAccomplishedCogsPont37 = matrix.brlAccomplishedCogsPont37;
		model.updatedMatrix.brlAccomplishedCogsPont49 = matrix.brlAccomplishedCogsPont49;
	
		// Valores Mensais confirme Prazo Contratual
		model.updatedMatrix.brlAccomplishedCogsMens12 = matrix.brlAccomplishedCogsMens12;
		model.updatedMatrix.brlAccomplishedCogsMens24 = matrix.brlAccomplishedCogsMens24;
		model.updatedMatrix.brlAccomplishedCogsMens36 = matrix.brlAccomplishedCogsMens36;
		model.updatedMatrix.brlAccomplishedCogsMens48 = matrix.brlAccomplishedCogsMens48;
		model.updatedMatrix.brlAccomplishedCogsMens60 = matrix.brlAccomplishedCogsMens60;
		
		/* USD */
		
		// CAPEX
		// Valores Pontuais
		model.updatedMatrix.usdAllocatedCapexInstall = matrix.usdAllocatedCapexInstall;
		model.updatedMatrix.usdAllocatedCapexPont1 = matrix.usdAllocatedCapexPont1;
		model.updatedMatrix.usdAllocatedCapexPont13 = matrix.usdAllocatedCapexPont13;
		model.updatedMatrix.usdAllocatedCapexPont25 = matrix.usdAllocatedCapexPont25;
		model.updatedMatrix.usdAllocatedCapexPont37 = matrix.usdAllocatedCapexPont37;
		model.updatedMatrix.usdAllocatedCapexPont49 = matrix.usdAllocatedCapexPont49;
	
		// Valores Mensais confirme Prazo Contratual
		model.updatedMatrix.usdAllocatedCapexMens12 = matrix.usdAllocatedCapexMens12;
		model.updatedMatrix.usdAllocatedCapexMens24 = matrix.usdAllocatedCapexMens24;
		model.updatedMatrix.usdAllocatedCapexMens36 = matrix.usdAllocatedCapexMens36;
		model.updatedMatrix.usdAllocatedCapexMens48 = matrix.usdAllocatedCapexMens48;
		model.updatedMatrix.usdAllocatedCapexMens60 = matrix.usdAllocatedCapexMens60;
		
		// OPEX
		// Valores Pontuais
		model.updatedMatrix.usdAllocatedOpexInstall = matrix.usdAllocatedOpexInstall;
		model.updatedMatrix.usdAllocatedOpexPont1 = matrix.usdAllocatedOpexPont1;
		model.updatedMatrix.usdAllocatedOpexPont13 = matrix.usdAllocatedOpexPont13;
		model.updatedMatrix.usdAllocatedOpexPont25 = matrix.usdAllocatedOpexPont25;
		model.updatedMatrix.usdAllocatedOpexPont37 = matrix.usdAllocatedOpexPont37;
		model.updatedMatrix.usdAllocatedOpexPont49 = matrix.usdAllocatedOpexPont49;
	
		// Valores Mensais confirme Prazo Contratual
		model.updatedMatrix.usdAllocatedOpexMens12 = matrix.usdAllocatedOpexMens12;
		model.updatedMatrix.usdAllocatedOpexMens24 = matrix.usdAllocatedOpexMens24;
		model.updatedMatrix.usdAllocatedOpexMens36 = matrix.usdAllocatedOpexMens36;
		model.updatedMatrix.usdAllocatedOpexMens48 = matrix.usdAllocatedOpexMens48;
		model.updatedMatrix.usdAllocatedOpexMens60 = matrix.usdAllocatedOpexMens60;
		
		// COGS
		// Valores Pontuais
		model.updatedMatrix.usdAllocatedCogsInstall = matrix.usdAllocatedCogsInstall;
		model.updatedMatrix.usdAllocatedCogsPont1 = matrix.usdAllocatedCogsPont1;
		model.updatedMatrix.usdAllocatedCogsPont13 = matrix.usdAllocatedCogsPont13;
		model.updatedMatrix.usdAllocatedCogsPont25 = matrix.usdAllocatedCogsPont25;
		model.updatedMatrix.usdAllocatedCogsPont37 = matrix.usdAllocatedCogsPont37;
		model.updatedMatrix.usdAllocatedCogsPont49 = matrix.usdAllocatedCogsPont49;
	
		// Valores Mensais confirme Prazo Contratual
		model.updatedMatrix.usdAllocatedCogsMens12 = matrix.usdAllocatedCogsMens12;
		model.updatedMatrix.usdAllocatedCogsMens24 = matrix.usdAllocatedCogsMens24;
		model.updatedMatrix.usdAllocatedCogsMens36 = matrix.usdAllocatedCogsMens36;
		model.updatedMatrix.usdAllocatedCogsMens48 = matrix.usdAllocatedCogsMens48;
		model.updatedMatrix.usdAllocatedCogsMens60 = matrix.usdAllocatedCogsMens60;
		
		// ACCOMPLISHED
		
		// CAPEX
		// Valores Pontuais
		model.updatedMatrix.usdAccomplishedCapexInstall = matrix.usdAccomplishedCapexInstall;
		model.updatedMatrix.usdAccomplishedCapexPont1 = matrix.usdAccomplishedCapexPont1;
		model.updatedMatrix.usdAccomplishedCapexPont13 = matrix.usdAccomplishedCapexPont13;
		model.updatedMatrix.usdAccomplishedCapexPont25 = matrix.usdAccomplishedCapexPont25;
		model.updatedMatrix.usdAccomplishedCapexPont37 = matrix.usdAccomplishedCapexPont37;
		model.updatedMatrix.usdAccomplishedCapexPont49 = matrix.usdAccomplishedCapexPont49;
	
		// Valores Mensais confirme Prazo Contratual
		model.updatedMatrix.usdAccomplishedCapexMens12 = matrix.usdAccomplishedCapexMens12;
		model.updatedMatrix.usdAccomplishedCapexMens24 = matrix.usdAccomplishedCapexMens24;
		model.updatedMatrix.usdAccomplishedCapexMens36 = matrix.usdAccomplishedCapexMens36;
		model.updatedMatrix.usdAccomplishedCapexMens48 = matrix.usdAccomplishedCapexMens48;
		model.updatedMatrix.usdAccomplishedCapexMens60 = matrix.usdAccomplishedCapexMens60;
		
		// OPEX
		// Valores Pontuais
		model.updatedMatrix.usdAccomplishedOpexInstall = matrix.usdAccomplishedOpexInstall;
		model.updatedMatrix.usdAccomplishedOpexPont1 = matrix.usdAccomplishedOpexPont1;
		model.updatedMatrix.usdAccomplishedOpexPont13 = matrix.usdAccomplishedOpexPont13;
		model.updatedMatrix.usdAccomplishedOpexPont25 = matrix.usdAccomplishedOpexPont25;
		model.updatedMatrix.usdAccomplishedOpexPont37 = matrix.usdAccomplishedOpexPont37;
		model.updatedMatrix.usdAccomplishedOpexPont49 = matrix.usdAccomplishedOpexPont49;
	
		// Valores Mensais confirme Prazo Contratual
		model.updatedMatrix.usdAccomplishedOpexMens12 = matrix.usdAccomplishedOpexMens12;
		model.updatedMatrix.usdAccomplishedOpexMens24 = matrix.usdAccomplishedOpexMens24;
		model.updatedMatrix.usdAccomplishedOpexMens36 = matrix.usdAccomplishedOpexMens36;
		model.updatedMatrix.usdAccomplishedOpexMens48 = matrix.usdAccomplishedOpexMens48;
		model.updatedMatrix.usdAccomplishedOpexMens60 = matrix.usdAccomplishedOpexMens60;
		
		// COGS
		// Valores Pontuais
		model.updatedMatrix.usdAccomplishedCogsInstall = matrix.usdAccomplishedCogsInstall;
		model.updatedMatrix.usdAccomplishedCogsPont1 = matrix.usdAccomplishedCogsPont1;
		model.updatedMatrix.usdAccomplishedCogsPont13 = matrix.usdAccomplishedCogsPont13;
		model.updatedMatrix.usdAccomplishedCogsPont25 = matrix.usdAccomplishedCogsPont25;
		model.updatedMatrix.usdAccomplishedCogsPont37 = matrix.usdAccomplishedCogsPont37;
		model.updatedMatrix.usdAccomplishedCogsPont49 = matrix.usdAccomplishedCogsPont49;
	
		// Valores Mensais confirme Prazo Contratual
		model.updatedMatrix.usdAccomplishedCogsMens12 = matrix.usdAccomplishedCogsMens12;
		model.updatedMatrix.usdAccomplishedCogsMens24 = matrix.usdAccomplishedCogsMens24;
		model.updatedMatrix.usdAccomplishedCogsMens36 = matrix.usdAccomplishedCogsMens36;
		model.updatedMatrix.usdAccomplishedCogsMens48 = matrix.usdAccomplishedCogsMens48;
		model.updatedMatrix.usdAccomplishedCogsMens60 = matrix.usdAccomplishedCogsMens60;
		
		model.action = 'update';
		model.isSuccess = false;
		model.isFailure = false;
	};
	model.isSelectedPackage = function(id) {
		if (model.updatedMatrix.package != null && id == model.updatedMatrix.package.id) {
			return true;
		}
		return false;
	};
	model.confirmUpdateMatrix = function() {
		var id = model.updatedMatrix.package;
		for (var i = 0; i < $scope.packages.length; i ++) {
			if (id == $scope.packages[i].id) {
				model.updatedMatrix.package = $scope.packages[i];
			}
		}
		service.httpPut('api/matrix/' + model.updatedMatrix.id, model.updatedMatrix, model.putCallback, model.generalFailureCallback);
		for (var i = 0; i <= $scope.matrixes.length; i ++) {
			if ($scope.matrixes[i].id == model.updatedMatrix.id) {
				$scope.matrixes[i].id = model.updatedMatrix.id;
				$scope.matrixes[i].name = model.updatedMatrix.name;
				$scope.matrixes[i].created = model.updatedMatrix.created;
				
				// BRL
				
				// ALLOCATED
				
				// CAPEX
				// Valores Pontuais
				$scope.matrixes[i].brlAllocatedCapexInstall = model.updatedMatrix.brlAllocatedCapexInstall;
				$scope.matrixes[i].brlAllocatedCapexPont1 = model.updatedMatrix.brlAllocatedCapexPont1;
				$scope.matrixes[i].brlAllocatedCapexPont13 = model.updatedMatrix.brlAllocatedCapexPont13;
				$scope.matrixes[i].brlAllocatedCapexPont25 = model.updatedMatrix.brlAllocatedCapexPont25;
				$scope.matrixes[i].brlAllocatedCapexPont37 = model.updatedMatrix.brlAllocatedCapexPont37;
				$scope.matrixes[i].brlAllocatedCapexPont49 = model.updatedMatrix.brlAllocatedCapexPont49;
			
				// Valores Mensais confirme Prazo Contratual
				$scope.matrixes[i].brlAllocatedCapexMens12 = model.updatedMatrix.brlAllocatedCapexMens12;
				$scope.matrixes[i].brlAllocatedCapexMens24 = model.updatedMatrix.brlAllocatedCapexMens24;
				$scope.matrixes[i].brlAllocatedCapexMens36 = model.updatedMatrix.brlAllocatedCapexMens36;
				$scope.matrixes[i].brlAllocatedCapexMens48 = model.updatedMatrix.brlAllocatedCapexMens48;
				$scope.matrixes[i].brlAllocatedCapexMens60 = model.updatedMatrix.brlAllocatedCapexMens60;
				
				// OPEX
				// Valores Pontuais
				$scope.matrixes[i].brlAllocatedOpexInstall = model.updatedMatrix.brlAllocatedOpexInstall;
				$scope.matrixes[i].brlAllocatedOpexPont1 = model.updatedMatrix.brlAllocatedOpexPont1;
				$scope.matrixes[i].brlAllocatedOpexPont13 = model.updatedMatrix.brlAllocatedOpexPont13;
				$scope.matrixes[i].brlAllocatedOpexPont25 = model.updatedMatrix.brlAllocatedOpexPont25;
				$scope.matrixes[i].brlAllocatedOpexPont37 = model.updatedMatrix.brlAllocatedOpexPont37;
				$scope.matrixes[i].brlAllocatedOpexPont49 = model.updatedMatrix.brlAllocatedOpexPont49;
			
				// Valores Mensais confirme Prazo Contratual
				$scope.matrixes[i].brlAllocatedOpexMens12 = model.updatedMatrix.brlAllocatedOpexMens12;
				$scope.matrixes[i].brlAllocatedOpexMens24 = model.updatedMatrix.brlAllocatedOpexMens24;
				$scope.matrixes[i].brlAllocatedOpexMens36 = model.updatedMatrix.brlAllocatedOpexMens36;
				$scope.matrixes[i].brlAllocatedOpexMens48 = model.updatedMatrix.brlAllocatedOpexMens48;
				$scope.matrixes[i].brlAllocatedOpexMens60 = model.updatedMatrix.brlAllocatedOpexMens60;
				
				// COGS
				// Valores Pontuais
				$scope.matrixes[i].brlAllocatedCogsInstall = model.updatedMatrix.brlAllocatedCogsInstall;
				$scope.matrixes[i].brlAllocatedCogsPont1 = model.updatedMatrix.brlAllocatedCogsPont1;
				$scope.matrixes[i].brlAllocatedCogsPont13 = model.updatedMatrix.brlAllocatedCogsPont13;
				$scope.matrixes[i].brlAllocatedCogsPont25 = model.updatedMatrix.brlAllocatedCogsPont25;
				$scope.matrixes[i].brlAllocatedCogsPont37 = model.updatedMatrix.brlAllocatedCogsPont37;
				$scope.matrixes[i].brlAllocatedCogsPont49 = model.updatedMatrix.brlAllocatedCogsPont49;
			
				// Valores Mensais confirme Prazo Contratual
				$scope.matrixes[i].brlAllocatedCogsMens12 = model.updatedMatrix.brlAllocatedCogsMens12;
				$scope.matrixes[i].brlAllocatedCogsMens24 = model.updatedMatrix.brlAllocatedCogsMens24;
				$scope.matrixes[i].brlAllocatedCogsMens36 = model.updatedMatrix.brlAllocatedCogsMens36;
				$scope.matrixes[i].brlAllocatedCogsMens48 = model.updatedMatrix.brlAllocatedCogsMens48;
				$scope.matrixes[i].brlAllocatedCogsMens60 = model.updatedMatrix.brlAllocatedCogsMens60;
				
				// ACCOMPLISHED
				
				// CAPEX
				// Valores Pontuais
				$scope.matrixes[i].brlAccomplishedCapexInstall = model.updatedMatrix.brlAccomplishedCapexInstall;
				$scope.matrixes[i].brlAccomplishedCapexPont1 = model.updatedMatrix.brlAccomplishedCapexPont1;
				$scope.matrixes[i].brlAccomplishedCapexPont13 = model.updatedMatrix.brlAccomplishedCapexPont13;
				$scope.matrixes[i].brlAccomplishedCapexPont25 = model.updatedMatrix.brlAccomplishedCapexPont25;
				$scope.matrixes[i].brlAccomplishedCapexPont37 = model.updatedMatrix.brlAccomplishedCapexPont37;
				$scope.matrixes[i].brlAccomplishedCapexPont49 = model.updatedMatrix.brlAccomplishedCapexPont49;
			
				// Valores Mensais confirme Prazo Contratual
				$scope.matrixes[i].brlAccomplishedCapexMens12 = model.updatedMatrix.brlAccomplishedCapexMens12;
				$scope.matrixes[i].brlAccomplishedCapexMens24 = model.updatedMatrix.brlAccomplishedCapexMens24;
				$scope.matrixes[i].brlAccomplishedCapexMens36 = model.updatedMatrix.brlAccomplishedCapexMens36;
				$scope.matrixes[i].brlAccomplishedCapexMens48 = model.updatedMatrix.brlAccomplishedCapexMens48;
				$scope.matrixes[i].brlAccomplishedCapexMens60 = model.updatedMatrix.brlAccomplishedCapexMens60;
				
				// OPEX
				// Valores Pontuais
				$scope.matrixes[i].brlAccomplishedOpexInstall = model.updatedMatrix.brlAccomplishedOpexInstall;
				$scope.matrixes[i].brlAccomplishedOpexPont1 = model.updatedMatrix.brlAccomplishedOpexPont1;
				$scope.matrixes[i].brlAccomplishedOpexPont13 = model.updatedMatrix.brlAccomplishedOpexPont13;
				$scope.matrixes[i].brlAccomplishedOpexPont25 = model.updatedMatrix.brlAccomplishedOpexPont25;
				$scope.matrixes[i].brlAccomplishedOpexPont37 = model.updatedMatrix.brlAccomplishedOpexPont37;
				$scope.matrixes[i].brlAccomplishedOpexPont49 = model.updatedMatrix.brlAccomplishedOpexPont49;
			
				// Valores Mensais confirme Prazo Contratual
				$scope.matrixes[i].brlAccomplishedOpexMens12 = model.updatedMatrix.brlAccomplishedOpexMens12;
				$scope.matrixes[i].brlAccomplishedOpexMens24 = model.updatedMatrix.brlAccomplishedOpexMens24;
				$scope.matrixes[i].brlAccomplishedOpexMens36 = model.updatedMatrix.brlAccomplishedOpexMens36;
				$scope.matrixes[i].brlAccomplishedOpexMens48 = model.updatedMatrix.brlAccomplishedOpexMens48;
				$scope.matrixes[i].brlAccomplishedOpexMens60 = model.updatedMatrix.brlAccomplishedOpexMens60;
				
				// COGS
				// Valores Pontuais
				$scope.matrixes[i].brlAccomplishedCogsInstall = model.updatedMatrix.brlAccomplishedCogsInstall;
				$scope.matrixes[i].brlAccomplishedCogsPont1 = model.updatedMatrix.brlAccomplishedCogsPont1;
				$scope.matrixes[i].brlAccomplishedCogsPont13 = model.updatedMatrix.brlAccomplishedCogsPont13;
				$scope.matrixes[i].brlAccomplishedCogsPont25 = model.updatedMatrix.brlAccomplishedCogsPont25;
				$scope.matrixes[i].brlAccomplishedCogsPont37 = model.updatedMatrix.brlAccomplishedCogsPont37;
				$scope.matrixes[i].brlAccomplishedCogsPont49 = model.updatedMatrix.brlAccomplishedCogsPont49;
			
				// Valores Mensais confirme Prazo Contratual
				$scope.matrixes[i].brlAccomplishedCogsMens12 = model.updatedMatrix.brlAccomplishedCogsMens12;
				$scope.matrixes[i].brlAccomplishedCogsMens24 = model.updatedMatrix.brlAccomplishedCogsMens24;
				$scope.matrixes[i].brlAccomplishedCogsMens36 = model.updatedMatrix.brlAccomplishedCogsMens36;
				$scope.matrixes[i].brlAccomplishedCogsMens48 = model.updatedMatrix.brlAccomplishedCogsMens48;
				$scope.matrixes[i].brlAccomplishedCogsMens60 = model.updatedMatrix.brlAccomplishedCogsMens60;
				
				/* USD */
				
				// CAPEX
				// Valores Pontuais
				$scope.matrixes[i].usdAllocatedCapexInstall = model.updatedMatrix.usdAllocatedCapexInstall;
				$scope.matrixes[i].usdAllocatedCapexPont1 = model.updatedMatrix.usdAllocatedCapexPont1;
				$scope.matrixes[i].usdAllocatedCapexPont13 = model.updatedMatrix.usdAllocatedCapexPont13;
				$scope.matrixes[i].usdAllocatedCapexPont25 = model.updatedMatrix.usdAllocatedCapexPont25;
				$scope.matrixes[i].usdAllocatedCapexPont37 = model.updatedMatrix.usdAllocatedCapexPont37;
				$scope.matrixes[i].usdAllocatedCapexPont49 = model.updatedMatrix.usdAllocatedCapexPont49;
			
				// Valores Mensais confirme Prazo Contratual
				$scope.matrixes[i].usdAllocatedCapexMens12 = model.updatedMatrix.usdAllocatedCapexMens12;
				$scope.matrixes[i].usdAllocatedCapexMens24 = model.updatedMatrix.usdAllocatedCapexMens24;
				$scope.matrixes[i].usdAllocatedCapexMens36 = model.updatedMatrix.usdAllocatedCapexMens36;
				$scope.matrixes[i].usdAllocatedCapexMens48 = model.updatedMatrix.usdAllocatedCapexMens48;
				$scope.matrixes[i].usdAllocatedCapexMens60 = model.updatedMatrix.usdAllocatedCapexMens60;
				
				// OPEX
				// Valores Pontuais
				$scope.matrixes[i].usdAllocatedOpexInstall = model.updatedMatrix.usdAllocatedOpexInstall;
				$scope.matrixes[i].usdAllocatedOpexPont1 = model.updatedMatrix.usdAllocatedOpexPont1;
				$scope.matrixes[i].usdAllocatedOpexPont13 = model.updatedMatrix.usdAllocatedOpexPont13;
				$scope.matrixes[i].usdAllocatedOpexPont25 = model.updatedMatrix.usdAllocatedOpexPont25;
				$scope.matrixes[i].usdAllocatedOpexPont37 = model.updatedMatrix.usdAllocatedOpexPont37;
				$scope.matrixes[i].usdAllocatedOpexPont49 = model.updatedMatrix.usdAllocatedOpexPont49;
			
				// Valores Mensais confirme Prazo Contratual
				$scope.matrixes[i].usdAllocatedOpexMens12 = model.updatedMatrix.usdAllocatedOpexMens12;
				$scope.matrixes[i].usdAllocatedOpexMens24 = model.updatedMatrix.usdAllocatedOpexMens24;
				$scope.matrixes[i].usdAllocatedOpexMens36 = model.updatedMatrix.usdAllocatedOpexMens36;
				$scope.matrixes[i].usdAllocatedOpexMens48 = model.updatedMatrix.usdAllocatedOpexMens48;
				$scope.matrixes[i].usdAllocatedOpexMens60 = model.updatedMatrix.usdAllocatedOpexMens60;
				
				// COGS
				// Valores Pontuais
				$scope.matrixes[i].usdAllocatedCogsInstall = model.updatedMatrix.usdAllocatedCogsInstall;
				$scope.matrixes[i].usdAllocatedCogsPont1 = model.updatedMatrix.usdAllocatedCogsPont1;
				$scope.matrixes[i].usdAllocatedCogsPont13 = model.updatedMatrix.usdAllocatedCogsPont13;
				$scope.matrixes[i].usdAllocatedCogsPont25 = model.updatedMatrix.usdAllocatedCogsPont25;
				$scope.matrixes[i].usdAllocatedCogsPont37 = model.updatedMatrix.usdAllocatedCogsPont37;
				$scope.matrixes[i].usdAllocatedCogsPont49 = model.updatedMatrix.usdAllocatedCogsPont49;
			
				// Valores Mensais confirme Prazo Contratual
				$scope.matrixes[i].usdAllocatedCogsMens12 = model.updatedMatrix.usdAllocatedCogsMens12;
				$scope.matrixes[i].usdAllocatedCogsMens24 = model.updatedMatrix.usdAllocatedCogsMens24;
				$scope.matrixes[i].usdAllocatedCogsMens36 = model.updatedMatrix.usdAllocatedCogsMens36;
				$scope.matrixes[i].usdAllocatedCogsMens48 = model.updatedMatrix.usdAllocatedCogsMens48;
				$scope.matrixes[i].usdAllocatedCogsMens60 = model.updatedMatrix.usdAllocatedCogsMens60;
				
				// ACCOMPLISHED
				
				// CAPEX
				// Valores Pontuais
				$scope.matrixes[i].usdAccomplishedCapexInstall = model.updatedMatrix.usdAccomplishedCapexInstall;
				$scope.matrixes[i].usdAccomplishedCapexPont1 = model.updatedMatrix.usdAccomplishedCapexPont1;
				$scope.matrixes[i].usdAccomplishedCapexPont13 = model.updatedMatrix.usdAccomplishedCapexPont13;
				$scope.matrixes[i].usdAccomplishedCapexPont25 = model.updatedMatrix.usdAccomplishedCapexPont25;
				$scope.matrixes[i].usdAccomplishedCapexPont37 = model.updatedMatrix.usdAccomplishedCapexPont37;
				$scope.matrixes[i].usdAccomplishedCapexPont49 = model.updatedMatrix.usdAccomplishedCapexPont49;
			
				// Valores Mensais confirme Prazo Contratual
				$scope.matrixes[i].usdAccomplishedCapexMens12 = model.updatedMatrix.usdAccomplishedCapexMens12;
				$scope.matrixes[i].usdAccomplishedCapexMens24 = model.updatedMatrix.usdAccomplishedCapexMens24;
				$scope.matrixes[i].usdAccomplishedCapexMens36 = model.updatedMatrix.usdAccomplishedCapexMens36;
				$scope.matrixes[i].usdAccomplishedCapexMens48 = model.updatedMatrix.usdAccomplishedCapexMens48;
				$scope.matrixes[i].usdAccomplishedCapexMens60 = model.updatedMatrix.usdAccomplishedCapexMens60;
				
				// OPEX
				// Valores Pontuais
				$scope.matrixes[i].usdAccomplishedOpexInstall = model.updatedMatrix.usdAccomplishedOpexInstall;
				$scope.matrixes[i].usdAccomplishedOpexPont1 = model.updatedMatrix.usdAccomplishedOpexPont1;
				$scope.matrixes[i].usdAccomplishedOpexPont13 = model.updatedMatrix.usdAccomplishedOpexPont13;
				$scope.matrixes[i].usdAccomplishedOpexPont25 = model.updatedMatrix.usdAccomplishedOpexPont25;
				$scope.matrixes[i].usdAccomplishedOpexPont37 = model.updatedMatrix.usdAccomplishedOpexPont37;
				$scope.matrixes[i].usdAccomplishedOpexPont49 = model.updatedMatrix.usdAccomplishedOpexPont49;
			
				// Valores Mensais confirme Prazo Contratual
				$scope.matrixes[i].usdAccomplishedOpexMens12 = model.updatedMatrix.usdAccomplishedOpexMens12;
				$scope.matrixes[i].usdAccomplishedOpexMens24 = model.updatedMatrix.usdAccomplishedOpexMens24;
				$scope.matrixes[i].usdAccomplishedOpexMens36 = model.updatedMatrix.usdAccomplishedOpexMens36;
				$scope.matrixes[i].usdAccomplishedOpexMens48 = model.updatedMatrix.usdAccomplishedOpexMens48;
				$scope.matrixes[i].usdAccomplishedOpexMens60 = model.updatedMatrix.usdAccomplishedOpexMens60;
				
				// COGS
				// Valores Pontuais
				$scope.matrixes[i].usdAccomplishedCogsInstall = model.updatedMatrix.usdAccomplishedCogsInstall;
				$scope.matrixes[i].usdAccomplishedCogsPont1 = model.updatedMatrix.usdAccomplishedCogsPont1;
				$scope.matrixes[i].usdAccomplishedCogsPont13 = model.updatedMatrix.usdAccomplishedCogsPont13;
				$scope.matrixes[i].usdAccomplishedCogsPont25 = model.updatedMatrix.usdAccomplishedCogsPont25;
				$scope.matrixes[i].usdAccomplishedCogsPont37 = model.updatedMatrix.usdAccomplishedCogsPont37;
				$scope.matrixes[i].usdAccomplishedCogsPont49 = model.updatedMatrix.usdAccomplishedCogsPont49;
			
				// Valores Mensais confirme Prazo Contratual
				$scope.matrixes[i].usdAccomplishedCogsMens12 = model.updatedMatrix.usdAccomplishedCogsMens12;
				$scope.matrixes[i].usdAccomplishedCogsMens24 = model.updatedMatrix.usdAccomplishedCogsMens24;
				$scope.matrixes[i].usdAccomplishedCogsMens36 = model.updatedMatrix.usdAccomplishedCogsMens36;
				$scope.matrixes[i].usdAccomplishedCogsMens48 = model.updatedMatrix.usdAccomplishedCogsMens48;
				$scope.matrixes[i].usdAccomplishedCogsMens60 = model.updatedMatrix.usdAccomplishedCogsMens60;
				
				break;
			}
		}
		model.action = 'view';
	};
	
	// Delete
	model.deleteMatrix = function(index) {
		var id = $scope.matrixes[index].id;
		service.httpDelete('api/matrix/' + id, model.deleteCallback, model.generalFailureCallback, index);
	};
	
	model.deleteMatrixes = function() {
		service.httpDeleteAll('api/matrix', model.deleteAllCallback, model.generalFailureCallback);
	};
	
	/*
	 * Callbacks
	 */
	
	model.getCallback = function(data) {
		
	};
	
	model.getAllCallback = function(data) {
		$scope.matrixes = data;
	};
	
	model.getAllPackageCallback = function(data) {
		$scope.packages = data;
	};
	
	model.postCallback = function(data) {
		$scope.matrixes = $scope.matrixes || [];
		$scope.matrixes.push(data);
		model.newMatrix.name = null;
		model.action = 'view';
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.putCallback = function(data) {
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.deleteCallback = function(data, index) {
		$scope.matrixes.splice(index, 1);
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.deleteAllCallback = function() {
		$scope.matrixes = [];
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.generalFailureCallback = function(data) {
		console.log('Failure');
		model.isSuccess = false;
		model.isFailure = true;
	};
	
	$scope.model = model;
	$scope.service = service;
});