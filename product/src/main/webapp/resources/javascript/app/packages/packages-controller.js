'use strict';
var model = model || {};
var service = service || {};
concilApp.controller('PackagesController', function($scope, AjaxService) {
	
	service = AjaxService;
	
	// Get All
	$scope.packages = [];
	service.init = function() {
		$scope.packages = service.httpGet('api/package', model.getAllCallback, model.generalFailureCallback);
		service.httpGet('api/product', model.getAllProductCallback, model.generalFailureCallback);
	}
	model.isSuccess = false;
	model.isFailure = false;
	
	// Post
	model.action = 'view';
	model.newPackage = {
		name: null,
		idProduct: null,
		volume: null,
		unit: null,
		price: null,
		recurrent: null,
		setup: null,
		custom: null,
		setupPrice: null,
		recurrencePrice: null
	};
	model.openCreatePackage = function() {
		model.action='create';
		model.isSuccess = false;
		model.isFailure = false;
	};
	model.createPackage = function() {
		var selectedProduct = null;
		for (var i = 0; i < $scope.products.length; i ++) {// All Products
			if ($scope.products[i].id == model.newPackage.product) {
				selectedProduct = $scope.products[i];
			}
		}
		model.newPackage.product = selectedProduct;
		
		service.httpPost('api/package', model.newPackage, model.postCallback, model.generalFailureCallback);
	}
	
	// Put
	model.updatedPackage = {
		id: null,
		name: null,
		volume: null,
		unit: null,
		price: null,
		recurrent: null,
		setup: null,
		custom: null,
		setupPrice: null,
		recurrencePrice: null,
		created: null
	};
	model.updatePackage = function(index) {
		var pack = $scope.packages[index];
		model.updatedPackage.id = pack.id;
		model.updatedPackage.name = pack.name;
		model.updatedPackage.product = pack.product;
		model.updatedPackage.volume = pack.volume;
		model.updatedPackage.unit = pack.unit;
		model.updatedPackage.price = pack.price;
		model.updatedPackage.recurrent = pack.recurrent;
		model.updatedPackage.setup = pack.setup;
		model.updatedPackage.custom = pack.custom;
		model.updatedPackage.setupPrice = pack.setupPrice;
		model.updatedPackage.recurrencePrice = pack.recurrencePrice;
		model.updatedPackage.created = pack.created;
		model.action = 'update';
		model.isSuccess = false;
		model.isFailure = false;
	};
	model.isSelectedProduct = function(id) {
		if (model.updatedPackage.product != null && id == model.updatedPackage.product.id) {
			return true;
		}
		return false;
	};
	model.confirmUpdatePackage = function() {
		var id = model.updatedPackage.product;
		for (var i = 0; i < $scope.products.length; i ++) {
			if (id == $scope.products[i].id) {
				model.updatedPackage.product = $scope.products[i];
			}
		}
		service.httpPut('api/package/' + model.updatedPackage.id, model.updatedPackage, model.putCallback, model.generalFailureCallback);
		for (var i = 0; i <= $scope.packages.length; i ++) {
			if ($scope.packages[i].id == model.updatedPackage.id) {
				$scope.packages[i].id = model.updatedPackage.id;
				$scope.packages[i].name = model.updatedPackage.name;
				$scope.packages[i].product = model.updatedPackage.product;
				$scope.packages[i].volume = model.updatedPackage.volume;
				$scope.packages[i].unit = model.updatedPackage.unit;
				$scope.packages[i].price = model.updatedPackage.price;
				$scope.packages[i].recurrent = model.updatedPackage.recurrent;
				$scope.packages[i].setup = model.updatedPackage.setup;
				$scope.packages[i].custom = model.updatedPackage.custom;
				$scope.packages[i].setupPrice = model.updatedPackage.setupPrice;
				$scope.packages[i].recurrencePrice = model.updatedPackage.recurrencePrice;
				$scope.packages[i].created = model.updatedPackage.created;
				break;
			}
		}
		model.action = 'view';
	};
	
	// Delete
	model.deletePackage = function(index) {
		var id = $scope.packages[index].id;
		service.httpDelete('api/package/' + id, model.deleteCallback, model.generalFailureCallback, index);
	};
	
	model.deletePackages = function() {
		service.httpDeleteAll('api/package', model.deleteAllCallback, model.generalFailureCallback);
	};
	
	/*
	 * Callbacks
	 */
	
	model.getCallback = function(data) {
		
	};
	
	model.getAllCallback = function(data) {
		$scope.packages = data;
	};
	
	model.getAllProductCallback = function(data) {
		$scope.products = data;
	};
	
	model.postCallback = function(data) {
		$scope.packages = $scope.packages || [];
		$scope.packages.push(data);
		model.newPackage.name = null;
		model.action = 'view';
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.putCallback = function(data) {
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.deleteCallback = function(data, index) {
		$scope.packages.splice(index, 1);
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.deleteAllCallback = function() {
		$scope.packages = [];
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.generalFailureCallback = function(data) {
		console.log('Failure');
		model.isSuccess = false;
		model.isFailure = true;
	};
	
	$scope.model = model;
	$scope.service = service;
});