concilApp.factory("AjaxService", function($http) {
	var service = {};
	
	// Exemplo: api/product/0
	service.httpGet = function(url, success, failure) {
		$http({
			method : 'GET',
			url: url
		}).then(function successCallback(response){
			if(success)
				success(response.data);
		}, function errorCallback(err){
			if(failure)
				failure(err);
		});
	};
	
	service.httpPost = function(url, data, success, failure) {
		$http({
			method: 'POST',
			url: url,
			data: data
		}).then(function successCallback(response) {
			if(success)
				success(response.data);
		}, function errorCallback(err) {
			if(failure)
				failure(err);
		});
	};
	
	service.httpPut = function(url, data, success, failure) {
		$http({
			method: 'PUT',
			url: url,
			data: data
		}).then(function successCallback(response) {
			if(success)
				success(response.data);
		}, function errorCallback(err) {
			if(failure)
				failure(err);
		});
	};
	
	service.httpDelete = function(url, success, failure, index) {
		$http({
			method: 'DELETE',
			url: url
		}).then(function successCallback(response) {
			if(success)
				success(response.data, index);
		}, function errorCallback(err) {
			if(failure)
				failure(err);
		});
	};
	
	service.httpDeleteAll = function(url, success, failure) {
		$http({
			method: 'DELETE',
			url: url
		}).then(function successCallback(response) {
			if(success)
				success(response.data);
		}, function errorCallback(err) {
			if(failure)
				failure(err);
		});
	};
	
	return service;
});