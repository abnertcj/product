'use strict';
var model = model || {};
var service = service || {};
concilApp.controller('GroupsController', function($scope, AjaxService) {
	
	service = AjaxService;
	
	// Get All
	$scope.groups = [];
	$scope.softwares = [];
	service.init = function() {
		service.httpGet('api/group', model.getAllCallback, model.generalFailureCallback);
		service.httpGet('api/software', model.getAllSoftwareCallback, model.generalFailureCallback);
	}
	model.isSuccess = false;
	model.isFailure = false;
	
	// Post
	model.action = 'view';
	model.newGroup = {
		name: null,
		softwares: null
	};
	model.openCreateGroup = function() {
		model.action='create';
		model.isSuccess = false;
		model.isFailure = false;
	};
	model.createGroup = function() {
		var selectedSoftwares = [];
		for (var i = 0; i < $scope.softwares.length; i ++) {//software in $scope.softwares) { // All Softwares
			if (model.newGroup.softwares != null) {
				for (var j = 0; j < model.newGroup.softwares.length; j ++) {//innerSoftwareId in model.newGroup.softwares) { // Selected Softwares
					if ($scope.softwares[i].id == model.newGroup.softwares[j]) {
						selectedSoftwares.push($scope.softwares[i]);
					}
				}
			}
		}
		model.newGroup.softwares = selectedSoftwares;
		service.httpPost('api/group', model.newGroup, model.postCallback, model.generalFailureCallback);
	}
	
	// Put
	model.updatedGroup = {
		id: null,
		name: null,
		softwares: [],
		created: null
	};
	model.updateGroup = function(index) {
		var group = $scope.groups[index];
		model.updatedGroup.id = group.id;
		model.updatedGroup.name = group.name;
		model.updatedGroup.softwares = group.softwares;
		model.updatedGroup.created = group.created;
		model.action = 'update';
		model.isSuccess = false;
		model.isFailure = false;
	};
	model.isSelectedSoftware = function(id) {
		for (var i = 0; i < model.updatedGroup.softwares.length; i ++) {
			if (id == model.updatedGroup.softwares[i].id) {
				return true;
			}
		}
		return false;
	};
	model.confirmUpdateGroup = function() {
		var softwaresFound = [];
		for (var i = 0; i < $scope.softwares.length; i ++) {
			for (var j = 0; j < model.updatedGroup.softwares.length; j ++) {
				if ($scope.softwares[i].id == model.updatedGroup.softwares[j]) {
					softwaresFound.push($scope.softwares[i]);
				}
			}
		}
		model.updatedGroup.softwares = softwaresFound;
		
		service.httpPut('api/group/' + model.updatedGroup.id, model.updatedGroup, model.putCallback, model.generalFailureCallback);
		for (var i = 0; i <= $scope.groups.length; i ++) {
			if ($scope.groups[i].id == model.updatedGroup.id) {
				$scope.groups[i].id = model.updatedGroup.id;
				$scope.groups[i].name = model.updatedGroup.name;
				$scope.groups[i].softwares = model.updatedGroup.softwares;
				$scope.groups[i].created = model.updatedGroup.created;
				break;
			}
		}
		model.action = 'view';
	};
	
	// Delete
	model.deleteGroup = function(index) {
		var id = $scope.groups[index].id;
		service.httpDelete('api/group/' + id, model.deleteCallback, model.generalFailureCallback, index);
	};
	
	model.deleteGroups = function() {
		service.httpDeleteAll('api/group', model.deleteAllCallback, model.generalFailureCallback);
	};
	
	/*
	 * Callbacks
	 */
	
	model.getCallback = function(data) {
		
	};
	
	model.getAllCallback = function(data) {
		$scope.groups = data;
	};
	
	model.getAllSoftwareCallback = function(data) {
		$scope.softwares = data;
	};
	
	model.postCallback = function(data) {
		$scope.groups = $scope.groups || [];
		$scope.groups.push(data);
		model.newGroup.name = null;
		model.action = 'view';
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.putCallback = function(data) {
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.deleteCallback = function(data, index) {
		$scope.groups.splice(index, 1);
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.deleteAllCallback = function() {
		$scope.groups = [];
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.generalFailureCallback = function(data) {
		console.log('Failure');
		model.isSuccess = false;
		model.isFailure = true;
	};
	
	$scope.model = model;
	$scope.service = service;
});