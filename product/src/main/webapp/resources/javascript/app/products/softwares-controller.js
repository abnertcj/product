'use strict';
var model = model || {};
var service = service || {};
concilApp.controller('SoftwaresController', function($scope, AjaxService) {
	
	service = AjaxService;
	
	// Get All
	$scope.softwares = [];
	service.init = function() {
		service.httpGet('api/software', model.getAllCallback, model.generalFailureCallback);
		service.httpGet('api/tax', model.getAllTaxCallback, model.generalFailureCallback);
		service.httpGet('api/addon', model.getAllAddonCallback, model.generalFailureCallback);
	}
	model.isSuccess = false;
	model.isFailure = false;
	
	// Post
	model.action = 'view';
	model.newSoftware = {
		product: {
			name: null
		},
		taxes: [],
		addons: []
	};
	model.openCreateSoftware = function() {
		model.action='create';
		model.isSuccess = false;
		model.isFailure = false;
	};
	model.createSoftware = function() {
		var selectedTaxes = [];
		for (var i = 0; i < $scope.taxes.length; i ++) {// All Taxes
			for (var j = 0; j < model.newSoftware.taxes.length; j ++) {// Selected Taxes
				if ($scope.taxes[i].id == model.newSoftware.taxes[j]) {
					selectedTaxes.push($scope.taxes[i]);
				}
			}
		}
		model.newSoftware.taxes = selectedTaxes;
		
		var selectedAddons = [];
		for (var i = 0; i < $scope.addons.length; i ++) {// All Addons
			for (var j = 0; j < model.newSoftware.addons.length; j ++) {// Selected Addons
				if ($scope.addons[i].id == model.newSoftware.addons[j]) {
					selectedAddons.push($scope.addons[i]);
				}
			}
		}
		model.newSoftware.addons = selectedAddons;
		service.httpPost('api/software', model.newSoftware, model.postCallback, model.generalFailureCallback);
	}
	
	// Put
	model.updatedSoftware = {
		id: null,
		product: {
			name: null,
			created: null
		},
		taxes: [],
		addons: []
	};
	model.updateSoftware = function(index) {
		var software = $scope.softwares[index];
		model.updatedSoftware.id = software.id;
		model.updatedSoftware.product.name = software.product.name;
		model.updatedSoftware.product.created = software.product.created;
		model.updatedSoftware.taxes = software.taxes;
		model.action = 'update';
		model.isSuccess = false;
		model.isFailure = false;
	};
	model.isSelectedTax = function(id) {
		for (var i = 0; i < model.updatedSoftware.taxes.length; i ++) {
			if (id == model.updatedSoftware.taxes[i].id) {
				return true;
			}
		}
		return false;
	};
	model.confirmUpdateSoftware = function() {
		var taxesFound = [];
		for (var i = 0; i < $scope.taxes.length; i ++) {
			for (var j = 0; j < model.updatedSoftware.taxes.length; j ++) {
				if ($scope.taxes[i].id == model.updatedSoftware.taxes[j]) {
					taxesFound.push($scope.taxes[i]);
				}
			}
		}
		model.updatedSoftware.taxes = taxesFound;
		
		var addonsFound = [];
		for (var i = 0; i < $scope.addons.length; i ++) {
			for (var j = 0; j < model.updatedSoftware.addons.length; j ++) {
				if ($scope.addons[i].id == model.updatedSoftware.addons[j]) {
					addonsFound.push($scope.addons[i]);
				}
			}
		}
		model.updatedSoftware.addons = addonsFound;
		
		service.httpPut('api/software/' + model.updatedSoftware.id, model.updatedSoftware, model.putCallback, model.generalFailureCallback);
		for (var i = 0; i <= $scope.softwares.length; i ++) {
			if ($scope.softwares[i].id == model.updatedSoftware.id) {
				$scope.softwares[i] = model.updatedSoftware;
				$scope.softwares[i].id = model.updatedSoftware.id;
				$scope.softwares[i].product.name = model.updatedSoftware.product.name;
				$scope.softwares[i].product.created = model.updatedSoftware.product.created;
				$scope.softwares[i].taxes = model.updatedSoftware.taxes;
				break;
			}
		}
		model.action = 'view';
	};
	
	// Delete
	model.deleteSoftware = function(index) {
		var id = $scope.softwares[index].id;
		service.httpDelete('api/software/' + id, model.deleteCallback, model.generalFailureCallback, index);
	};
	
	model.deleteSoftwares = function() {
		service.httpDeleteAll('api/software', model.deleteAllCallback, model.generalFailureCallback);
	};
	
	/*
	 * Callbacks
	 */
	
	model.getCallback = function(data) {
		
	};
	
	model.getAllCallback = function(data) {
		$scope.softwares = data;
	};
	
	model.getAllTaxCallback = function(data) {
		$scope.taxes = data;
	};
	
	model.getAllAddonCallback = function(data) {
		$scope.addons = data;
	};
	
	model.postCallback = function(data) {
		$scope.softwares = $scope.softwares || [];
		$scope.softwares.push(data);
		model.newSoftware.name = null;
		model.action = 'view';
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.putCallback = function(data) {
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.deleteCallback = function(data, index) {
		$scope.softwares.splice(index, 1);
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.deleteAllCallback = function() {
		$scope.softwares = [];
		model.isSuccess = true;
		model.isFailure = false;
	};
	
	model.generalFailureCallback = function(data) {
		console.log('Failure');
		model.isSuccess = false;
		model.isFailure = true;
	};
	
	$scope.model = model;
	$scope.service = service;
});