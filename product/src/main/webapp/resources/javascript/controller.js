'use strict';
concilApp.controller('TestController', function($scope, ConcilService) {
	var model = model || {};
	var service = service || {};
	
	model.name = null;
	
	service = ConcilService;
	var controller = this;
	
	controller.log = function() {
		console.log('Log!');
	};
	
	$scope.products;
	
	service.init = function() {
		$scope.products = service.getAll(model.callback);
	}
	
	model.callback = function(data) {
		$scope.products = data;
//		alert("I'm the callback!");
	}
	
	$scope.model = model;
	$scope.service = service;
	
});